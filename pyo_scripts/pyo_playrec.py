import pyo
import numpy as np
from time import sleep

s = pyo.Server(nchnls=1).boot()
s.start()

print "playrec now"
# OUTPUT
filename = "../multitone/funnyerror.wav"
out_info = pyo.sndinfo(filename)
[n_frames, duration, s_rate, n_channels, f_format, s_type] = out_info
outp = pyo.SfPlayer(filename, loop=False, mul=0.6)

# INPUT
inp = pyo.Input(chnl=0)
# better not to out() this if you don't want feedback
rec = pyo.Record(inp, "pyrec.wav", chnls=1, fileformat=0)

# START

pyo.CallAfter([outp.out(), sleep(duration), pyo.Clean_objects(1.0, rec).start() ], time=[1, 2, 3]) # executes the stop after the duration in seconds
print "done"

pyo.CallAfter([s.stop(), pyo.Clean_objects(1.0,s)], time = [duration+1, duration+2 ])
print "...quit!"

