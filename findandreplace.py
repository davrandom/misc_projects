# -*- coding: UTF-8 -*-
# define our method
f = open('/home/davide.scaini/Documents/davstuff/europa/dueruoteismeglcheuan.tex', 'r').read()

def replace_all(text, dic):
    for i, j in dic.iteritems():
        text = text.replace(i, j)
    return text

# our dictionary with our key:values.
# we want to replace 'H' with '|-|'
# 'e' with '3' and 'o' with '0'
reps = {'perche\'':'perché', 'e\'':'è', 'o\'':'ò','a\'':'à', 'u\'':'ù', 'i\'':'ì'}
rev = {'pò':'po\'','à\'':'a\'\'','ò\'':'o\'\'','ù\'':'u\'\'','è\'':'e\'\'','é\'':'e\'\'','ì\'':'i\'\''}
rev2 = {'o\'\'\'':'ò\'\''}
m = replace_all(f,reps)
m = replace_all(m,rev)
m = replace_all(m,rev2)
print m

out = open('/home/davide.scaini/Documents/davstuff/europa/temp.tex', 'w')
out.write(m)
out.close()
