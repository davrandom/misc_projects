---
layout: default
title: "TODOs"
comments: false
sharing: false
footer: true
permalink: todos/
---


List of TODOs _not_ (in order of priority).

# Overall

* find a **name** for the thing
* write a **CLI** to set up the whole thing
* interface in low priority wrt CLI
* config files to tune the desired output 
(just text / jekyll website / jekyll with ot without maps/plots/...)

## Reports (Jekyll)

* paginate posts page
* find a nice and clear theme
* automate the _config.yml to reflect athlete's name


## Stats (pygpxstats)

* bokeh plots
* be able to parse fit and tcx files (they probably carry 
the description you write in strava)
* change "not_moving" (now based on instantaneous speed_km_h < 0.5) 
to some moving average of speed or accumulated distance
