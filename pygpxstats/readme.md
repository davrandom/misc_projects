# pygpxstats

## Goals

The aim of pygpxstats is to create markdown (MD) text files from gpx files.
These MD files will contain some statistics calculated over the gpx data,
similar to what is done by training applications (such as Strava, Endomondo, etc).
The generated files should be general enough to be plugged into the
most common static website generators, such as Jekyll, Pelican, etc.

The goal is to create an open source framework to analise training data
and rely on other opensource frameworks for visualisation and navigation.


## Pillars

There shall be no databases!

Everything should be designed to be static.
Text files and proper folder hierarchy have to be sufficient.

### Parts that compose the whole

* {0.1} **stats** (pygpxstats)
* {0.1} **reports** (jekill)
* {_dev} **ui** \[_common, _desktop, _mobile\] (enaml, enaml-native)

"reports" is a jekyll blog:

```
jekyll new reports
```

## Examples and Tests

Tests should be simple and clear. They should serve also as minimal examples for someone that wants to understand and hack the code.


## Dependancies
```
gpxpy (parsing gpx)
firparse (parsing fit files)
numpy (arrays - I want to remove this)
matplotlib (plots - I want to remove this in favour of bokeh)
geopy (calculate distances)
jekyll (blog)
folium (maps)
bokeh (web plots)
```

Python:
```
python3 -m pip install gpxpy numpy matplotlib geopy folium
```


## Usage
```
python3 stats/pygpxstats.py gpx_filename.gpx
```

or read the help:
```
python3 pygpxstats.py -h
```

if you have jekyll intialized in "reports" folder, you can do
```
python3 stats/pygpxstats.py gpx_files/Commute_to_work.gpx -o reports/_posts/
```
