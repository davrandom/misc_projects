from __future__ import print_function

import datetime
import sys
from configparser import SafeConfigParser, ParsingError
import os
import json

from atom.api import Atom, Unicode, Range, Bool, Value, Int, Tuple, observe, ContainerList
import enaml
from enaml.qt.qt_application import QtApplication


class Person(Atom):
    """
    A simple class representing a person object.
    """
    surname = Unicode()
    name = Unicode()
    age = Range(low=0)
    birthday = Value(datetime.date(1985, 1, 1))

    @observe('birthday')
    def update_age(self, change):
        """
        Update the person's age whenever their date of birth changes
        """
        # grab the current date time
        today = datetime.datetime.utcnow()
        # estimate the person's age within one year accuracy
        age = today.year - self.birthday.year
        # check to see if the current date is before their birthday and
        # subtract a year from their age if it is
        if today.month < self.birthday.month or (today.month == self.birthday.month and today.day < self.birthday.day):
            age -= 1
        # set the persons age
        self.age = age


class Athlete(Person):
    """
    An athlete is person with a sport and a(t least one) gear.
    """
    # The athlete's sport
    sports = ContainerList(default=[])

    cycling = Value(Bool(False))
    running = Value(Bool(False))

    def save_to_file(self):
        print("saving to file...")
        # TODO move this somewhere else... (clean)
        file_name = 'user_data.ini'
        file_handle = open(file_name, 'w')
        file_handle.write("[UserInfo]\n")
        file_handle.write("name = %s\n" % self.name)
        file_handle.write("surname = %s\n" % self.surname)
        file_handle.write("year = %s\n" % self.birthday.year)
        file_handle.write("month = %s\n" % self.birthday.month)
        file_handle.write("day = %s\n" % self.birthday.day)
        file_handle.write("\n[Sports]\n")
        file_handle.write("sports = %s\n" % list_to_string(self.sports, ', '))
        file_handle.close()

        # TODO: now kills the program, in the future it should go to the next view
        sys.exit()


def list_to_string(names_list, delimiter=' '):
    out = ''
    num = len(names_list)-1
    for idx, item in enumerate(names_list):
        out += '%s' % item
        if idx < num:
            out += delimiter
    return out


def parse_user_info_file(file_name, athlete_instance):
    parser = SafeConfigParser()
    parser.read(file_name)

    athlete_instance.name = parser.get('UserInfo', 'name')
    athlete_instance.surname = parser.get('UserInfo', 'surname')
    year  = parser.getint('UserInfo', 'year')
    month = parser.getint('UserInfo', 'month')
    day   = parser.getint('UserInfo', 'day')
    athlete_instance.birthday = datetime.datetime(year, month, day)

    sports_string = parser.get('Sports', 'sports')
    athlete_instance.sports = sports_string.split(', ')


def main():
    app = QtApplication()

    # Create an athlete with a sport
    athlete_ = Athlete(name='John', surname='Doe')

    file_name = 'user_data.ini'
    file_exists = os.path.isfile(file_name)
    if file_exists:
        try:
            parse_user_info_file(file_name, athlete_)
            print("all good")
        except:
            print("File not found or not properly formatted.")
            file_exists = False
            pass

    if not file_exists:
        # TODO: if 'user_data.ini' doesn't exist then
        # Import our Enaml AthleteView
        with enaml.imports():
            from user_data_ui import AthleteView

        # Create a view and show it.
        view = AthleteView(athlete=athlete_)
        view.show()
        app.start()

    # TODO otherwise just load 'user_data.ini' and continue
    with enaml.imports():
        from user_data_ui import RecordView

    view = RecordView(athlete=athlete_)
    view.show()

    app.start()


if __name__ == '__main__':
    main()


