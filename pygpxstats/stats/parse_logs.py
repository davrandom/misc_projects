import gpxpy
import gpxpy.gpx
import fitparse as fp


class DataContainer:
    def __init__(self):
        self.list_of_variables = {'lat': False, 'lon': False, 'ele': False, 'date': False,
                                  'hr': False, 'cad': False, 'atemp': False, 'grade': False}


class GpxParserElementTree:

    def __init__(self, gpx_file_name):
        # TODO generalize namespace :P
        self.namespace = {'web': 'http://www.topografix.com/GPX/1/1',
                          'garmin': 'http://www.garmin.com/xmlschemas/TrackPointExtension/v1'}

        print("Start parsing %s ..." % gpx_file_name)

        self.gpx_file = open(gpx_file_name, 'r')
        gpx = gpxpy.parse(self.gpx_file)

        self.current_track_idx = 0
        self.current_segment_idx = 0
        self.current_point_idx = 0

        try:
            self.tracks = gpx.tracks
            self.total_number_of_tracks = len(self.tracks)

            self.current_track = self.tracks[self.current_track_idx]
            self.current_track_name = self.current_track.name

            self.segments = self.current_track.segments
            self.current_segment = self.segments[self.current_segment_idx]
            self.points = self.current_segment.points
            self.current_point = self.points[self.current_point_idx]
        except ValueError:
            raise ValueError("File is malformed.")

        self.number_of_segments_for_current_track = len(self.segments)
        self.number_of_points_for_current_segment = len(self.points)

        self.list_of_variables = {'lat': False, 'lon': False, 'ele': False, 'date': False,
                                  'hr': False, 'cad': False, 'atemp': False, 'grade': False}

    def close_file(self):
        self.gpx_file.close()

    def _update_track_info(self):
        self.current_track = self.tracks[self.current_track_idx]
        self.current_track_name = self.current_track.name
        self.segments = self.current_track.segments
        self.points = self.current_segment.points
        self.number_of_segments_for_current_track = len(self.segments)

    def _update_segment_info(self):
        self.current_segment = self.segments[self.current_segment_idx]
        self.number_of_points_for_current_segment = len(self.points)

    def _update_point_info(self):
        self.current_point = self.points[self.current_point_idx]

    def get_current_point(self):
        # and return point information
        lat = float(self.current_point.latitude)
        lon = float(self.current_point.longitude)

        if lat: self.list_of_variables['lat'] = True
        if lon: self.list_of_variables['lon'] = True

        ele = 0
        date = 0
        hr = 0
        cad = 0
        atemp = 0

        ele = self.current_point.elevation
        if ele is not None:
            self.list_of_variables['ele'] = True

        date = self.current_point.time
        if date is not None:
            self.list_of_variables['time'] = True

        extension = self.current_point.extensions
        if (type(extension) is list) and extension:
            has_hr = extension[0].find('garmin:hr', self.namespace)
            if has_hr is not None:
                hr = int(has_hr.text)
                self.list_of_variables['hr'] = True
            has_cad = extension[0].find('garmin:cad', self.namespace)
            if has_cad is not None:
                cad = int(has_cad.text)
                self.list_of_variables['cad'] = True
            has_temp = extension[0].find('garmin:atemp', self.namespace)
            if has_temp is not None:
                atemp = int(has_temp.text)
                self.list_of_variables['atemp'] = True

        return {'lat': lat, 'lon': lon, 'ele': ele, 'date': date, 'hr': hr, 'cad': cad, 'atemp': atemp}
        # fixme I don't like this. temporary // return an instance of DataContainer

    def get_next_point(self, verbose=False):
        self.current_point_idx += 1

        if self.current_point_idx >= self.number_of_points_for_current_segment:
            # if you reached the last point in segment, reset points counter
            self.current_point_idx = 0
            # and increment segments counter
            self.current_segment_idx += 1

        if self.current_segment_idx >= self.number_of_segments_for_current_track:
            # if you reached the last segment in the track, reset segments counter
            self.current_segment_idx = 0
            # and increment track counter
            self.current_track_idx += 1
        else:
            self._update_segment_info()

        if self.current_track_idx >= self.total_number_of_tracks:
            # if you reached the last track in the gpx file, then you are done
            print('Reached end of file.')
            return 'eof'
        else:
            # otherwise update all the information
            self._update_track_info()

        self._update_point_info()
        if verbose:
            print('track: %d/%d segment: %d/%d point: %d/%d' % (self.current_track_idx+1, self.total_number_of_tracks,
                                                                self.current_segment_idx+1, self.number_of_segments_for_current_track,
                                                                self.current_point_idx+1, self.number_of_points_for_current_segment))
        current = self.get_current_point()
        return current



# TODO: make a function that un-gz-s files

# TODO: make a similar class for fit files

class FitParser:
    def __init__(self, fit_file_name):


        self.sport_type = None
        self.workout_type = None

        self.list_of_variables = {'lat': False, 'lon': False, 'ele': False, 'date': False,
                                  'hr': False, 'cad': False, 'atemp': False, 'grade': False}

        print("Start parsing %s ..." % fit_file_name)
        # self.gpx_file = open(fit_file_name, 'r')
        fit_dictionary = self._parse_a_fit_file(fit_file_name)

        self.current_point_idx = 0
        self.points = fit_dictionary
        self.current_point = self.points[self.current_point_idx]
        self.current_track_name = fit_file_name

    def get_current_point(self):
        return self.points[self.current_point_idx]

    def get_next_point(self):
        self.current_point_idx += 1
        return self.points[self.current_point_idx]

    def close_file(self):
        return True

    def _parse_a_fit_file(self, filename):
        # dataff = fp.FitFile('activities/1159050001.fit')
        dataff = fp.FitFile(filename)

        events = dataff.get_messages('event')
        file_id = dataff.get_messages('file_id')
        device_info = dataff.get_messages('device_info')
        sport = dataff.get_messages('sport')
        workout = dataff.get_messages('workout')
        record = dataff.get_messages('record')

        self.sport_type = sport
        self.workout_type = workout

        # for info in events:
        #     fields = info.fields
        #     for field in fields:
        #         print('%s: %s' % (field.name, field.value))
        #
        # for info in file_id:
        #     fields = info.fields
        #     for field in fields:
        #         print('%s: %s' % (field.name, field.value))
        #
        # for info in device_info:
        #     fields = info.fields
        #     for field in fields:
        #         print('%s: %s' % (field.name, field.value))
        #
        # for info in sport:
        #     fields = info.fields
        #     for field in fields:
        #         print('%s: %s' % (field.name, field.value))
        #
        # for info in workout:
        #     fields = info.fields
        #     for field in fields:
        #         print('%s: %s' % (field.name, field.value))

        list_of_dictionaries_with_data = []
        for record in dataff.get_messages('record'):
            # Go through all the data entries in this record
            this_dict = {}
            this_dict['hr'] = 0
            this_dict['cad'] = 0
            this_dict['ele'] = 0
            this_dict['date'] = None
            this_dict['lat'] = None
            this_dict['lon'] = None

            for record_data in record:
                # Print the records name and value (and units if it has any)
                # if record_data.units:
                #     print("%s: %s %s" % (record_data.name, record_data.value, record_data.units))
                # else:
                #     print("%s: %s" % (record_data.name, record_data.value))

                if record_data.name == 'timestamp' and record_data.value:
                    this_dict['date'] = record_data.value

                if record_data.name == 'altitude' and record_data.value:
                    this_dict['ele'] = record_data.value

                if record_data.name == 'distance' and record_data.value:
                    this_dict[record_data.name] = record_data.value

                if record_data.name == 'grade' and record_data.value:
                    this_dict[record_data.name] = record_data.value

                if record_data.name == 'heart_rate':
                    hr_value = record_data.value
                    if not hr_value: hr_value = 0
                    this_dict['hr'] = hr_value

                if record_data.name == 'cadence':
                    cad_value = record_data.value
                    if not cad_value: cad_value = 0
                    this_dict['cad'] = cad_value

                if record_data.name == 'position_lat' and record_data.value:
                    latitude_semicircle = float(record_data.value)
                    latitude_degree = latitude_semicircle * (180. / 2 ** 31)
                    this_dict['lat'] = latitude_degree

                if record_data.name == 'position_long' and record_data.value:
                    latitude_semicircle = float(record_data.value)
                    latitude_degree = latitude_semicircle * (180. / 2 ** 31)
                    this_dict['lon'] = latitude_degree

            if this_dict['date'] and this_dict['lat'] and this_dict['lon']:
                # make sure you have lat long and date
                list_of_dictionaries_with_data.append(this_dict)
            else:
                print("Skipping point.")

        list_of_dictionaries_with_data.append('eof')
        return list_of_dictionaries_with_data


# fixme: in fit file missing cad, atemp
# return {'lat': lat, 'lon': lon, 'ele': ele, 'date': date, 'hr': hr, 'cad': cad, 'atemp': atemp}

# >> data_message.get_values()
# {
#     'altitude': 24.6,
#     'cadence': 97,
#     'distance': 81.97,
#     'grade': None,
#     'heart_rate': 153,
#     'position_lat': None,
#     'position_long': None,
#     'power': None,
#     'resistance': None,
#     'speed': 7.792,
#     'temperature': 20,
#     'time_from_course': None,
#     'timestamp': datetime.datetime(2011, 11, 6, 13, 41, 50)
# }

