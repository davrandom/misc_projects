import logs_statistics as st
import plot_stats as pt

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as pyp
import matplotlib.patches as mpatches
import folium
import re


def summary(accumulate_instance, output_type='terminal'):
    a = accumulate_instance

    # Summaries
    # remove from calculations points you don't trust
    trust_list = np.array(a.trust_list)
    delta_time_list = np.array(a.delta_time_list)
    distance_geo_list_m = np.array(a.distance_geo_list_m)
    distance_ele_list_m = np.array(a.distance_ele_list_m)
    speed_km_h_list = np.array(a.speed_km_h_list)
    pace_s_km = np.array(a.pace_s_km)

    # raw data
    total_distance_geodesic = np.sum(distance_geo_list_m)
    total_distance_with_ele = np.sum(distance_ele_list_m)

    # corrected (compare with raw)
    moving_tot_distance_geo = np.sum(distance_geo_list_m[trust_list])
    moving_tot_distance_ele = np.sum(distance_ele_list_m[trust_list])


    # Total
    dict_summary_total = {}
    dict_summary_total['total'] = ["Summary (total):", "value", "unit"]

    start_date = a.points_list[0]['date']
    stop_date = a.points_list[-1]['date']
    total_elapsed_time_date = stop_date - start_date
    total_elapsed_time_s = total_elapsed_time_date.total_seconds()
    total_elapsed_time_min_sec = st.from_s_to_h_m_s(total_elapsed_time_s)

    total_avg_speed_m_s = moving_tot_distance_geo / total_elapsed_time_s
    total_avg_speed_km_h = total_avg_speed_m_s * 3.6
    total_pace_s_km = total_elapsed_time_s / (moving_tot_distance_geo / 1000)
    total_pace_min_km = st.from_pace_s_km_to_pace_min_km(total_pace_s_km)

    dict_summary_total['total_distance']  = ["Total distance:", "%.2f" % (moving_tot_distance_geo / 1000), "km"]
    dict_summary_total['total_time']      = ["Total time:", "%d:%d:%02d" % total_elapsed_time_min_sec, "hh:mm:ss"]
    dict_summary_total['total_avg_speed'] = ["Average speed:", "%.1f" % total_avg_speed_km_h, "km/h"]
    dict_summary_total['total_avg_pace']  = ["Average pace:", "%d:%02d" % total_pace_min_km, "min/km"]
    dict_summary_total['date'] = [start_date, stop_date]

    # Moving
    dict_summary_moving = {}
    dict_summary_moving['moving'] = ["Summary (moving):", "value", "unit"]

    moving_time = np.sum(delta_time_list[trust_list])
    moving_time_min_sec = st.from_s_to_h_m_s(moving_time)
    moving_avg_speed_km_h = np.mean(speed_km_h_list[trust_list])
    moving_avg_pace_s_km = np.mean(pace_s_km[trust_list])
    moving_avg_pace_min_km = st.from_pace_s_km_to_pace_min_km(moving_avg_pace_s_km)

    # fixme: move this somewhere else >>>
    elevation_list = np.array(a.ele_list)
    trusted_elevation_list = elevation_list[trust_list]
    ele_diffs = [trusted_elevation_list[idx+1] - trusted_elevation_list[idx] for idx in range(len(trusted_elevation_list)-1)]
    positive_ele_diffs = [el for idx, el in enumerate(ele_diffs) if el > 0]
    # fixme: move this somewhere else <<<

    accumulated_elevation_in_meters = np.sum(positive_ele_diffs)

    dict_summary_moving['moving_distance']  = ["Total distance:", "%.2f" % (moving_tot_distance_geo / 1000), "km"]
    dict_summary_moving['moving_time']      = ["Moving time:", "%d:%d:%02d" % moving_time_min_sec, "hh:mm:ss"]
    dict_summary_moving['moving_avg_speed'] = ["Average moving speed:", "%.1f" % moving_avg_speed_km_h, "km/h"]
    dict_summary_moving['moving_avg_pace']  = ["Average moving pace:", "%d:%02d" % moving_avg_pace_min_km, "min/km"]
    dict_summary_moving['elevation_estimation'] = ["Total estimated accumulated elevation:", "%d" % accumulated_elevation_in_meters, "meters"]
    dict_summary_moving['heart_rate_mean'] = ["Mean heart rate:", "%d" % np.mean(a.hr_list), "bpm"]

    # Pauses
    string_pauses = ''
    how_long = delta_time_list[np.logical_not(trust_list)]
    string_pauses += "Application paused %d times: \n" % len(how_long)
    for idx, pauselength in enumerate(how_long):
        h, m, s = st.from_s_to_h_m_s(pauselength)
        string_pauses += "%d) %d:%02d \n" % (idx+1, m, s)

    how_long = delta_time_list[a.not_moving_list]  # this might work by chance (by the fact that app is autopausing)
    string_pauses += "You made %d pauses: \n" % len(how_long)
    for idx, pauselength in enumerate(how_long):
        h, m, s = st.from_s_to_h_m_s(pauselength)
        string_pauses += "%d) %d:%02d \n" % (idx+1, m, s)

    # experimenting
    stopped_list = np.array(a.stopped_list)
    stopped_s = np.sum(delta_time_list[stopped_list])
    stopped_h_m_s = st.from_s_to_h_m_s(stopped_s)

    return dict_summary_total, dict_summary_moving, string_pauses


def gpx_map(accumulate_instance, output_type='mthl4markdown'):
    a = accumulate_instance

    lat_center = sum(lat for lat in a.lat_list) / len(a.lat_list)
    lon_center = sum(lon for lon in a.lon_list) / len(a.lon_list)
    points_list = [tuple([lat, a.lon_list[idx]]) for idx, lat in enumerate(a.lat_list)]

    # Load map centred on average coordinates
    my_map = folium.Map(location=[lat_center, lon_center], zoom_start=14)

    # add markers
    start_point = points_list[0]
    end_point = points_list[-1]

    start_icon = folium.map.Icon(color='green', icon_color='white', icon='play')
    stop_icon  = folium.map.Icon(color='red', icon_color='white', icon='stop')
    folium.Marker(start_point, popup='Start', icon=start_icon).add_to(my_map)
    folium.Marker(end_point, popup='End', icon=stop_icon).add_to(my_map)

    # fadd lines
    folium.PolyLine(points_list, color="red", weight=2.5, opacity=1).add_to(my_map)

    # get html
    html_string = my_map.get_root().render()

    # hacky parsing of html
    lines = html_string.split('\n')
    # ignore everything in lines until <body>
    interesting_lines = []
    trash = True
    inside_style = False
    for line in lines:
        if '<style>#map_' in line:
            trash = False
            inside_style = True

        if 'body>' in line:
            trash = False  # skips everything before body
            interesting_lines.append('\n')
            continue  # skips body closure

        if 'head>' in line:
            continue

        if not trash:
            add_line = line
            if 'var ' in line or '<div ' in line or 'style>' in line:
                add_line = line.strip()

            if inside_style and 'height: 100.0%;' in line:
                add_line = ' height: 400px;'
                inside_style = False

            if add_line:
                add_line = re.sub(' +', ' ', add_line)
                add_line = add_line.rstrip()
                interesting_lines.append(add_line)

    return interesting_lines


def figures_default_set(accumulate_instance, out_path=''):
    a = accumulate_instance

    delta_time_list = np.array(a.delta_time_list)
    speed_km_h_list = np.array(a.speed_km_h_list)
    trust_list = np.array(a.trust_list)

    # PLOTTING
    # plotting raw stuff
    # pt.simple_plot_variable(a.ele_list, 'elevation', out_path)
    pt.simple_plot_two_variables([idx for idx in range(len(a.ele_list))], a.ele_list, 'elevation', 'point index', 'meters', out_path)
    pt.simple_plot_two_variables([idx for idx in range(len(delta_time_list))], delta_time_list, 'delta time', 'point index', 'seconds', out_path)

    pt.simple_plot_two_variables(a.absolute_time_list, a.ele_list, 'elevation (meters)', 'seconds', 'meters', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.distance_geo_list_m, 'distance (meters)', 'seconds', 'meters', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.speed_m_s_list, 'speed (m/s)', 'seconds', 'm/s', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.speed_km_h_list, 'speed (km/h)', 'seconds', 'km/h', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.accel_list, 'acceleration (m/s^2)', 'seconds', 'm/s^2', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.cad_list, 'cadence (rmp)', 'seconds', 'rounds per minute or steps per minute', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.hr_list, 'heart rate (bpm)', 'seconds', 'beats per minute', out_path)

    # highlighting "wrong" points (tbd)
    absolute_time_list = np.array(a.absolute_time_list)
    not_moving_list = np.array(a.not_moving_list)

    var_name = 'speed'
    x = np.array(absolute_time_list)
    y = np.array(speed_km_h_list)
    x_label = 'seconds'
    y_label = 'm/s'

    fig, ax = pyp.subplots()
    pyp.plot(x, y)
    if var_name:
        pyp.legend([var_name])
    else:
        var_name = 'default_name'
    if x_label:
        pyp.xlabel(x_label)
    if y_label:
        pyp.ylabel(y_label)

    # pyp.plot(x, trust_list)
    # pyp.plot(x[np.logical_not(trust_list)], y[np.logical_not(trust_list)])
    where_stop = absolute_time_list[np.logical_not(trust_list)]
    how_long = delta_time_list[np.logical_not(trust_list)]
    for idx, whe in enumerate(where_stop):
        patch = mpatches.Rectangle((whe-how_long[idx], np.min(y)), how_long[idx], np.max(y)*1.1+0.1, angle=0.0, color='r', ec="none", alpha=0.3)
        #    Draw a rectangle with lower left at xy = (x, y) with specified width, height and rotation angle.
        ax.add_patch(patch)

    where_nm = absolute_time_list[not_moving_list]
    how_long = delta_time_list[not_moving_list]
    for idx, whe in enumerate(where_nm):
        patch = mpatches.Rectangle((whe-how_long[idx], np.min(y)), how_long[idx], np.max(y)*1.1+0.1, angle=0.0, color='g', ec="none", alpha=0.3)
        #    Draw a rectangle with lower left at xy = (x, y) with specified width, height and rotation angle.
        ax.add_patch(patch)

    pyp.axis('auto')
    pyp.tight_layout()

    # pyp.show()
    pyp.savefig(os.path.join(out_path, var_name + '.png'))
    pyp.close()


def bokeh_figures_simple_example(accumulate_instance, out_path=''):
    a = accumulate_instance

    output_string = ''

    script, div = pt.bokeh_simple_plot_two_variables(a.absolute_time_list, a.ele_list, 'elevation (meters)', 'seconds', 'meters', out_path)

    output_string += div + '\n'
    output_string += script + '\n'

    return output_string


def bokeh_figures_default_set(accumulate_instance, out_path=''):
    a = accumulate_instance

    output_string = ''

    list_of_dicts = []
    dict_ele = {'x': a.absolute_time_list, 'y': a.moving_ele_list, 'var_name': 'elevation (meters)', 'x_label': 'seconds', 'y_label': 'meters'}
    list_of_dicts.append(dict_ele)

    if any([hr > 10 for hr in a.hr_list]):
        dict_hr = {'x': a.absolute_time_list, 'y': a.moving_hr_list, 'var_name': 'heart-rate (beats-per-minute)', 'x_label': 'seconds', 'y_label': 'beats-per-minute'}
        list_of_dicts.append(dict_hr)

    dict_speed = {'x': a.absolute_time_list, 'y': a.moving_speed_km_h_list, 'var_name': 'speed (km/h)', 'x_label': 'seconds', 'y_label': 'km/h'}
    list_of_dicts.append(dict_speed)
    # OR append pace if you are running...

    script, div = pt.bokeh_multiplot_from_list_dictionary(list_of_dicts, out_path)

    output_string += div + '\n'
    output_string += script + '\n'

    return output_string


class WriteToMarkdownFile:
    def __init__(self, filename):
        self.filehandle = open(filename, 'w+')

    def header(self, layout='post', title='Title', category='', comments='true'):
        self.write_this('---\n')
        self.write_this('layout: %s\n' % layout)
        self.write_this('title: "%s"\n' % title)
        self.write_this('categories: %s\n' % category)
        self.write_this('tags: %s\n' % category)
        self.write_this('comments: %s\n' % comments)
        self.write_this('---\n\n')

    def close(self):
        self.filehandle.close()

    def write_this(self, string):
        self.filehandle.write(string)

    @staticmethod
    def make_table_out_of_dictionary(dictionary):
        values = dictionary.values()
        table = ''
        for idx, val in enumerate(values):
            if len(val) == 3:
                table += "| %s | %s | %s | \n" % (val[0], val[1], val[2])
                if idx == 0:
                    table += "| :--- | ---: | :--- | \n"

        return table


def print_dictionary(dictionary):
    values = dictionary.values()
    for val in values:
        if len(val) == 3:
            print("%s \t%s \t%s" % (val[0], val[1], val[2]))

    print("\n")


def write_dictionary_to_txt_file(list_of_dictionaries, filename):
    file_handle = open(filename, 'w')

    for dictionary in list_of_dictionaries:
        values = dictionary.values()
        for val in values:
            if len(val) == 3:
                file_handle.write("%s \t%s \t%s\n" % (val[0], val[1], val[2]))

    file_handle.close()


def read_txt_file_to_dictionary(filename):
    file_handle = open(filename, 'r')
    dictionary = {}
    for line in file_handle:
        name, value, unit = line.split('\t')
        name = name.replace(': ', '')
        unit = unit.replace('\n', '')
        dictionary[name] = [value, unit]

    file_handle.close()
    return dictionary



