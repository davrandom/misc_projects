import matplotlib.pyplot as pyp
from bokeh.embed import components
from bokeh.plotting import figure, show, output_file
from bokeh.models import LinearAxis, Range1d
import os


def simple_plot_variable(variable, var_name='', out_path=''):
    pyp.figure()
    pyp.plot(variable)
    if var_name:
        pyp.legend([var_name])
    else:
        var_name = 'default_name'
    # pyp.show()
    pyp.savefig(os.path.join(out_path, var_name+'.png'))
    pyp.close()


def simple_plot_two_variables(x, y, var_name='', x_label='', y_label='', out_path=''):
    pyp.figure()
    pyp.plot(x, y)
    if var_name:
        pyp.legend([var_name])
    else:
        var_name = 'default_name'
    if x_label:
        pyp.xlabel(x_label)
    if y_label:
        pyp.ylabel(y_label)

    # pyp.show()
    # replace weird characters in string
    mydict = {" ": "_", "/": "_", "^": "_", "(": "", ")": ""}
    for old, new in mydict.items():
        var_name = var_name.replace(old, new)

    pyp.savefig(os.path.join(out_path, var_name+'.png'))
    pyp.close()


def bokeh_simple_plot_two_variables(x, y, var_name='', x_label='', y_label='', out_path=''):
    # replace weird characters in string
    mydict = {" ": "_", "/": "_", "^": "_", "(": "", ")": ""}
    for old, new in mydict.items():
        var_name = var_name.replace(old, new)

    p1 = figure(x_axis_type="datetime", title=var_name)
    p1.grid.grid_line_alpha = 0.3
    p1.xaxis.axis_label = x_label
    p1.yaxis.axis_label = y_label

    p1.line(x, y, color='#A6CEE3', legend=var_name)
    p1.legend.location = "top_left"

    script, div = components(p1)
    return script, div


def bokeh_multiplot_from_list_dictionary(list_of_dicts, title='TBD', out_path=''):
    # dict keys: x, y, var_name='', x_label='', y_label=''

    list_of_colors = ['#A6CEE3', '#B2DF8A', '#33A02C', '#FB9A99']
    list_of_colors = ['gray', 'red', 'black', '#FB9A99']
    where_axis = ['left', 'right', 'right']
    p1 = figure(x_axis_type="datetime", title=title)
    p1.grid.grid_line_alpha = 0.3

    if len(list_of_dicts) > 1:
        p1.extra_y_ranges = {}

    for idx, dict in enumerate(list_of_dicts):
        x = dict['x']
        y = dict['y']
        var_name = dict['var_name']
        x_label = dict['x_label']
        y_label = dict['y_label']

        # replace weird characters in string
        # mydict = {" ": "_", "/": "_", "^": "_", "(": "", ")": "", "-": "_"}
        # for old, new in mydict.items():
        #     var_name = var_name.replace(old, new)
        #     y_label = y_label.replace(old, new)

        p1.xaxis.axis_label = x_label
        min_y = 0
        max_y = max(y)*1.2

        if idx > 0:
            range_name = y_label+'_range'
            p1.extra_y_ranges[range_name] = Range1d(start=min_y, end=max_y)
            p1.add_layout(LinearAxis(y_range_name=range_name, axis_label=y_label), where_axis[idx])
            p1.line(x, y, color=list_of_colors[idx], legend=var_name, y_range_name=range_name)
        else:
            p1.yaxis.axis_label = y_label
            p1.y_range = Range1d(start=min_y, end=max_y)
            p1.line(x, y, color=list_of_colors[idx], legend=var_name)

    p1.legend.location = "top_left"

    # show(p1)

    script, div = components(p1)
    return script, div