import os
import output_stats as op
import pandas as pd
import datetime as dt

# go through all years and all folders
# and sum up


calendar_path = 'calendar'

day_distance = []
day_elevation = []
day_total_time_s = []
day_moving_time_s = []

month_distance = []
month_elevation = []
month_total_time_s = []
month_moving_time_s = []

year_distance = []
year_elevation = []
year_total_time_s = []
year_moving_time_s = []

years = os.listdir(calendar_path)
years = sorted(years)
years.reverse()

all_dates = []
all_day_distance = []
all_day_elevation = []
all_day_total_time_s = []
all_day_moving_time_s = []


for year in years:
    #
    month_path = os.path.join(calendar_path, year)
    if not os.path.isdir(month_path):
        continue
    months = os.listdir(month_path)
    months = sorted(months)
    months.reverse()
    #
    month_distance = []
    month_elevation = []
    month_total_time_s = []
    month_moving_time_s = []

    for month in months:
        #
        day_path = os.path.join(calendar_path, year, month)
        if not os.path.isdir(day_path):
            continue
        days = os.listdir(day_path)
        days = sorted(days)
        days.reverse()
        #

        day_distance = []
        day_elevation = []
        day_total_time_s = []
        day_moving_time_s = []

        for day in days:
            #
            activity_path = os.path.join(calendar_path, year, month, day)
            if not os.path.isdir(activity_path):
                continue
            activities = os.listdir(activity_path)
            activities = [act for act in activities if '.txt' in act]
            print('day %s (%s %s) activities: %s' % (day, month, year, activities))
            #
            for activity in activities:
                #
                name, extension = os.path.splitext(activity)
                if not 'txt' in extension:
                    continue

                activity_dict = op.read_txt_file_to_dictionary(os.path.join(activity_path, activity))

                distance    = float(activity_dict['Total distance'][0])
                elevation   = float(activity_dict['Total estimated accumulated elevation'][0])
                total_time  = activity_dict['Total time'][0]
                moving_time = activity_dict['Moving time'][0]

                total_time_s = total_time.split(':')
                total_time_s = 3600 * int(total_time_s[0]) + 60 * int(total_time_s[1]) + int(total_time_s[2])

                moving_time_s = total_time.split(':')
                moving_time_s = 3600 * int(moving_time_s[0]) + 60 * int(moving_time_s[1]) + int(moving_time_s[2])

                day_distance.append(distance)
                day_elevation.append(elevation)
                day_total_time_s.append(total_time_s)
                day_moving_time_s.append(moving_time_s)

                date = dt.date(int(year), int(month), int(day))
                all_dates.append(date)
                all_day_distance.append(distance)
                all_day_elevation.append(elevation)
                all_day_total_time_s.append(total_time_s)
                all_day_moving_time_s.append(moving_time_s)

            if len(days) == len(day_distance):
                print('This month %s daily log (%s)' % (month, year))
                print('days: %s' % days)
                print('day_distance: %s' % day_distance)
                print('day_elevation: %s' % day_elevation)
                print('day_total_time_s: %s' % day_total_time_s)
                print('day_moving_time_s: %s' % day_moving_time_s)

        month_distance.append(sum(day_distance))
        month_elevation.append(sum(day_elevation))
        month_total_time_s.append(sum(day_total_time_s))
        month_moving_time_s.append(sum(day_moving_time_s))

        if len(months) == len(month_distance):
            print('This year %s monthly log' % year)
            print('months: %s' % months)
            print('month_distance: %s' % month_distance)
            print('month_elevation: %s' % month_elevation)
            print('month_total_time_s: %s' % month_total_time_s)
            print('month_moving_time_s: %s' % month_moving_time_s)

    year_distance.append(sum(month_distance))
    year_elevation.append(sum(month_elevation))
    year_total_time_s.append(sum(month_total_time_s))
    year_moving_time_s.append(sum(month_moving_time_s))

    if len(years) == len(year_distance):
        print('year %s' % year)
        print('years: %s' % years)
        print('year_distance: %s' % year_distance)
        print('year_elevation: %s' % year_elevation)
        print('year_total_time_s: %s' % year_total_time_s)
        print('year_moving_time_s: %s' % year_moving_time_s)


a = 1

myd = {
    'distance' : pd.Series(all_day_distance, index=all_dates),
    'elevation' : pd.Series(all_day_elevation, index=all_dates),
    'total_time' : pd.Series(all_day_total_time_s, index=all_dates),
    'moving_time' : pd.Series(all_day_moving_time_s, index=all_dates),
    }

# d = {'one' : pd.Series([1., 2., 3.], index=['a', 'b', 'c']),
#      'two' : pd.Series([1., 2., 3., 4.], index=['a', 'b', 'c', 'd'])}

df = pd.DataFrame(myd)
df.index = pd.to_datetime(df.index)  # convert index to datetime
# esempio
# df['2018-06-23':'2018-06-21']
df.to_pickle('calendar_data.pkl')
df.to_csv('calendar_data.cvs')

