import parse_logs as pa
import logs_statistics as st
import output_stats as op

import os
from argparse import ArgumentParser
import gzip
import shutil


force_analysis = True


def parse_and_write_plus_plot(gpslog_file_name, out_path=None):

    filename = os.path.split(gpslog_file_name)[-1]
    name, extension = os.path.splitext(filename)

    # todo: if gpx
    if extension == '.gpx':
        # raise ValueError("I want to process fit files :D")
        prsr = pa.GpxParserElementTree(gpslog_file_name)
    # todo: if fit
    elif extension == '.fit':
        prsr = pa.FitParser(gpslog_file_name)
    else:
        raise ValueError("Can't process this filetype %s. I can only process .gpx and .fit" % extension)

    if not out_path:
        out_path = name + '_files'

    is_out_path_existent = os.path.isdir(out_path)
    if not is_out_path_existent:
        os.makedirs(out_path)

    point = prsr.get_current_point()

    if point == 'eof':
        return False, False

    list_of_variables = prsr.list_of_variables
    initial_date = point['date']

    calendar_path = 'calendar'
    source, destination = make_calendar_folder_hierarchy(initial_date, gpslog_file_name, calendar_path)
    abs_activity_name, gpxextension = os.path.splitext(destination)

    is_activity_summary = is_activity_summary_existent(abs_activity_name+'.txt')
    if is_activity_summary and not force_analysis:
        prsr.close_file()
        return False, False

    der = st.InstantDerivedQuantities(point)
    der.push_new_observable_list(point)  # point zero (you don't calculate stuff here)
    point = prsr.get_next_point()  # here we go!

    t0 = point['date'].timestamp()

    a = st.Accumulate()
    a.set_initial_timestamp(t0)

    counter = 1
    while True:
        if point == 'eof':
            break
        # save current data
        a.push_original_data(point)

        # calculate RT stuff
        der.push_and_calculate(point)

        # save derived quantities
        a.push_derived_quantities(der)

        point = prsr.get_next_point()
        counter += 1

    prsr.close_file()

    # output modules
    total_summary, moving_summary, pauses = op.summary(a)  # get stats summary
    op.figures_default_set(a, out_path)  # spit some plots

    op.print_dictionary(total_summary)
    op.print_dictionary(moving_summary)

    op.write_dictionary_to_txt_file([total_summary, moving_summary], abs_activity_name+'.txt')
    # readdict = op.read_txt_file_to_dictionary(abs_activity_name+'.txt')

    map_html = op.gpx_map(a)

    head_md_name = "%s-%s-%s-" % (initial_date.year, initial_date.month, initial_date.day)
    mdfile = os.path.join(out_path, head_md_name+name+'.md')
    md = op.WriteToMarkdownFile(mdfile)
    md.header(title=prsr.current_track_name, category='mtb collserola')

    # maps and plots
    md.write_this('\n[//]: # "html code for the map"\n')
    for line in map_html:
        md.write_this(line)
        md.write_this("\n")
    md.write_this("\n")

    # tables
    md.write_this('\n[//]: # "tables"\n\n')
    total_table = md.make_table_out_of_dictionary(total_summary)
    md.write_this(total_table)
    md.write_this("\n")
    moving_table = md.make_table_out_of_dictionary(moving_summary)
    md.write_this(moving_table)

    # bokeh plots
    plot_string = op.bokeh_figures_default_set(a, out_path)
    md.write_this('\n[//]: # "html code for the plots"')
    md.write_this(plot_string)

    md.close()

    return total_summary, moving_summary


def make_calendar_folder_hierarchy(initial_date, gpx_absolute_filename, calendar_path):
    # TODO check types: initial_date has to be a datetime instance
    import datetime
    if type(initial_date) is not datetime.datetime:
        raise TypeError("initial_date should be a datetime instance.")

    filename = gpx_absolute_filename
    fn = os.path.split(filename)[-1]
    name, extension = os.path.splitext(fn)

    out_path = calendar_path
    is_out_path_existent = os.path.isdir(out_path)
    if not is_out_path_existent:
        os.makedirs(out_path)

    year_path  = os.path.join(out_path, str(initial_date.year))
    month_path = os.path.join(out_path, str(initial_date.year), '%02d' % initial_date.month)
    day_path   = os.path.join(out_path, str(initial_date.year), '%02d' % initial_date.month, '%02d' % initial_date.day)

    if not os.path.isdir(year_path):
        os.makedirs(year_path)

    if not os.path.isdir(month_path):
        os.makedirs(month_path)

    if not os.path.isdir(day_path):
        os.makedirs(day_path)

    # here (this folder)
    pwd = os.getcwd()
    # create symlink to original file in calendar structure
    src = os.path.join(pwd, filename)
    des = os.path.join(pwd, day_path, fn)
    is_symlink_exists = os.path.islink(des)
    if not is_symlink_exists:
        os.symlink(src, des)

    return src, des


def is_activity_summary_existent(txt_absolute_filename):
    is_activity_file = os.path.isfile(txt_absolute_filename)
    return is_activity_file


if __name__ == "__main__":
    parser = ArgumentParser(description="Parse gpx and create graphs and stats")
    parser.add_argument('gpxfile', metavar='gpxfile', type=str, help="path and name of your gpx file")
    parser.add_argument('-o', metavar='output_destination', type=str, help="destination path for generated output",
                        default='.')

    args = parser.parse_args()

    gpx_file_name = args.gpxfile
    output_dest = args.o

    is_gpx_file_name_isfile = os.path.isfile(gpx_file_name)
    is_gpx_file_name_isdir  = os.path.isdir(gpx_file_name)
    if is_gpx_file_name_isfile:
        # if gpx_file_name is_file then run once parser
        filename = os.path.split(gpx_file_name)[-1]
        name, extension = os.path.splitext(filename)
        updated_output_dest = os.path.join(output_dest, name + '_files')

        total_summary, moving_summary = parse_and_write_plus_plot(gpx_file_name, updated_output_dest)

    elif is_gpx_file_name_isdir:
        # if gpx_file_name is_directory the go through all the gpx files in the dir
        # parse_and_write_plus_plot(gpx_file_name, output_dest)
        list_of_files = os.listdir(gpx_file_name)
        for subfile in list_of_files:
            name, extension = os.path.splitext(subfile)

            # uncompress activities
            if extension == ".gz":
                if name not in list_of_files:
                    with gzip.open(os.path.join(gpx_file_name, subfile), 'rb') as f_in:
                        with open(os.path.join(gpx_file_name, name), 'wb') as f_out:
                            shutil.copyfileobj(f_in, f_out)
                            print("Unzipped %s." % subfile)

                            subfile = name
                            name, extension = os.path.splitext(subfile)
                else:
                    continue

            if extension == ".gpx" or extension == ".fit":
                file_to_process = os.path.join(gpx_file_name, subfile)

                filename = os.path.split(subfile)[-1]
                name, extension = os.path.splitext(filename)
                updated_output_dest = os.path.join(output_dest, name + '_files')

                try:
                    total_summary, moving_summary = parse_and_write_plus_plot(file_to_process, updated_output_dest)
                except Exception:
                    print("Failed to parse %s file." % subfile)
                    raise ValueError("debug me")
                    pass

    else:
        raise ValueError("Your gpxfile is wrong and can't be processed.")
