# you need an API key !
import geocoder


def replace_elevation(list_of_points):
    for point in list_of_points:
        ele = geocoder.google([point['lat'], point['lon']], method='elevation').meters
        point['ele'] = ele

# or look into this:
# https://github.com/tkrajina/srtm.py
