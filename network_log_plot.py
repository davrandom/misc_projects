import numpy as np
import matplotlib.pyplot as plt
import sys

class Parser() :

    # function for parsing the data
    def _data_parser(self, text, dic):
        ## ignores all the lines that do not contain as firts word the one carried by "area"
        ## all the others are processed following the rules in dictionary (dic) 
        ## the text is then splitted into words and returned as an array
        tmp = text.split()
        if len(tmp)>0 : 
            for i, j in dic.iteritems():
                text = text.replace(i,j)
            text = text.split()
            return text
        else:
            return None


    def parse(self, my_text):
        ## method that does the actual processing 
        
        # replacing strange characters 
        dic = {'^I':' ','--':'NaN','\n':''}

        for line in my_text:
            parsed = self._data_parser(text=line, dic=dic)
            if parsed != None:
                if any(np.asarray(parsed) == 'CEST'):
                    if not self.date.size: # if empty
                        self.date       = np.asarray(parsed)
                    else:
                        self.date       = np.vstack((self.date, parsed))
                if any(np.asarray(parsed) == 'Ping:'):
                    if not self.latency.size: # if empty
                        self.latency    = np.asarray(parsed)
                    else:
                        self.latency    = np.vstack((self.latency, parsed ))
                if any(np.asarray(parsed) == 'Download:'):
                    if not self.download.size: # if empty
                        self.download   = np.asarray(parsed)
                    else:
                        self.download   = np.vstack((self.download, parsed ))
                if any(np.asarray(parsed) == 'Upload:'):
                    if not self.upload.size: # if empty
                        self.upload     = np.asarray(parsed)
                    else:
                        self.upload     = np.vstack((self.upload, parsed ))



    def __init__(self, filename):
        
        self.date       = np.array([])
        self.latency    = np.array([])
        self.download   = np.array([])
        self.upload     = np.array([])

        self.inputfile = open(filename)
        my_text = self.inputfile.readlines()
        self.parse(my_text)


# here set the folder where speed.txt is located
prefix = "./"

### box
## freq and phase
tfile    = Parser(prefix+"speed.txt")
if len(tfile.latency) != len(tfile.download) or len(tfile.latency) != len(tfile.upload) or len(tfile.upload) != len(tfile.download):
    print "Parsing is giving different length for different objects. Exit."
    sys.exit()


last_sample = len(tfile.upload)
#### plotting
fig = plt.figure()
ax  = plt.gca()

rect = fig.patch
rect.set_facecolor('w')

ax.grid(linestyle=':')
ax.set_xlabel('Samples')
ax.set_ylabel('Value [ms] [Mbit/s]')
ax.set_title("Network statistics")
plt.ylim(0,300)

lat = plt.plot(  tfile.latency[:,1].astype(np.float) , color='r', linestyle=':', label='latency [ms]')
dow = plt.plot(  tfile.download[:,1].astype(np.float) , color='k', label='download [Mbit/s]')
upl = plt.plot(  tfile.upload[:,1].astype(np.float) , color='g', label='upload [Mbit/s]')

plt.legend(['latency [ms]','download [Mbit/s]','upload [Mbit/s]'], loc='upper left')

plt.savefig('networklog_full.png')

plt.xlim(last_sample-2880, last_sample)
ax.set_title("Network statistics - last 30 days")
plt.savefig('networklog_lastMonth.png')

plt.xlim(last_sample-672, last_sample)
ax.set_title("Network statistics - last 7 days")
plt.savefig('networklog_lastWeek.png')

plt.xlim(last_sample-96, last_sample)
ax.set_title("Network statistics - last 24 hours")
plt.savefig('networklog_lastDay.png')
