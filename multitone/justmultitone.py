import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


T = 10.0        ## time window
fstart = 20.0   ## starting frequency
fstop = 4000.0 ## stop frequency 
R = 1.0/3.0        ## resolution (now in freq put in octaves!!!)
samplingf = 48000
# my opinion:  
# the phase parameters are a bit tricky to choose. anyway you want a to be big for the phase to rotate "fast" 
# and wrap as much as possible and have the different frequencies sum up incoherently as much as possible
phi0 = np.pi/7.0      ## initial phase
m = 2.0*np.pi/5.0   ## phase mod
a = 100.0*np.pi/3.0         ## unknown parameter (how fast you want to rotate the phase?)

## wtf is T??? see p64 part9 Klippel seminar. No idea.
## anyway I implemented it as in the slide an I interpret it as a f bias... that should "randomize" a bit the frequency
fbias = 0.345334455

## wtf is U(fi)? see p64 part 9 Klippel, is it a normalization factor for each frequency? if yes what should I use?

t = np.arange(0.0, T, 1.0/samplingf)
u = 0
f = 100.0
u = np.zeros(len(t))

phii = phi0
fi = 0
i = 0.0
## figures
fig = plt.figure()
while fi < fstop:
    fi = (1.0/fbias)* int(fbias* fstart * pow(2, i*R)) 
    newf = 1.0* np.cos(2.0*np.pi* fi * t + phii)
    plt.plot(t[1:480],newf[1:480])
    u = u + newf
    phii =  ((a*phii*m)/(2.0*np.pi)) % m
    phii = phii * 2.0*np.pi/m 
    print fi, phii
    i += 1.0

plt.show()
u = u/max(u)*0.9

## figures
fig = plt.figure()
plt.plot(t[1:4800],u[1:4800])
plt.show()

## fourier transform
fig2 = plt.figure()
ax = plt.gca()
uspect = np.fft.fft(u, n=48000)
uspect = np.fft.fft(u)
freq = np.fft.fftfreq(t.shape[-1])
plt.semilogx()
ticks = [15, 30, 60, 125, 250, 500, 1000, 2000, 4000, 8000, 16000]
plt.xticks(ticks,ticks)

rect = fig.patch
rect.set_facecolor('w')

ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Gain/Phase')
ax.set_title('Multitone signal Fourier transform')
ax.grid(linestyle=':')
ax.set_xlim(10,24000)

plt.plot(samplingf*np.abs(freq[0:]), np.abs(uspect.real), samplingf*np.abs(freq[0:]), uspect.imag)

plt.show()

## write wav
from wavefile import WaveWriter, Format
# you can download and compile wavfile from https://github.com/vokimon/python-wavefile
import numpy as np

with WaveWriter('multitone.ogg', channels=1, samplerate=48000, format=Format.OGG|Format.VORBIS) as w :
    w.metadata.title = "Some Noise"
    w.metadata.artist = "The Artists"
    data = np.zeros((1,len(u)), np.float32)
    data[0,:] = u
    w.write(data)



wait = raw_input()
