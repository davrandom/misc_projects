import numpy as np
import matplotlib.pyplot as plt
import ipdb

class Parser() :

    # function for parsing the data
    def _data_parser(self, text, dic):
        ## ignores all the lines that do not contain as firts word the one carried by "area"
        ## all the others are processed following the rules in dictionary (dic) 
        ## the text is then splitted into words and returned as an array
        tmp = text.split()
        if len(tmp)>0 : 
            # here I know that the word I'm searching for is in column 1
            for i, j in dic.iteritems():
                text = text.replace(i,j)
            text = text.split()
            return text
        else:
            return None


    def parse(self, my_text):
        ## method that does the actual processing 
        dic = {'^I':' ','--':'NaN'}

        tmp = np.array([])
        count = 0
        for line in my_text:
            parsed = self._data_parser(text=line, dic=dic)
            if parsed != None:
                if count == 0: tmp = parsed; count += 1
                else:          tmp = np.vstack((tmp,parsed))

        ## getting only the values I'm interested in...
        self.freq   = tmp[:,0].astype(np.float)
        self.dB     = tmp[:,1].astype(np.float)
        self.phase  = tmp[:,2].astype(np.float)


    def __init__(self, filename):
        self.inputfile = open(filename)
        my_text = self.inputfile.readlines()
        self.parse(my_text)



prefix = "./sica/"

### box
## freq and phase
tfile    = Parser(prefix+"sica_40Hz.txt")
f3 = tfile.freq[np.isclose(tfile.dB,-3,atol=0.1)]
dB3 = tfile.dB[np.isclose(tfile.dB,-3,atol=0.1)]
### transducer
## freq response and inductance
#ipdb.set_trace()




#### plotting
fig = plt.figure()
ax  = plt.gca()

markers = [10, 20, 30, 40, 50 , 100, 300, 500, 1000]
lmarkers = ('10', '20', '30', '40', '50', '100', '300', '500', '1000')
rect = fig.patch
rect.set_facecolor('w')

ax.grid(linestyle=':')
ax.set_xscale('log')
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Gain [dB]')
ax.set_title("Enclosure effect")
plt.xlim(10,1000)
plt.ylim(-30,6)

## box freq
plt.plot(tfile.freq,tfile.dB,color='k')
sf3 = 'f3 = %.1f Hz' % f3
plt.text(f3,dB3+4,sf3,horizontalalignment='center')
## transducer freq response

## crossover freq response

## convolution of the two and the thre and the threee


plt.plot([10,1000],[0,0],color='g',linestyle='-')
plt.plot([10,1000],[-1,-1],color='k',linestyle=':')
plt.plot([10,1000],[-2,-2],color='k',linestyle=':')
plt.plot([10,1000],[-3,-3],color='r',linestyle='-')
plt.xticks(markers,lmarkers)
plt.show()
