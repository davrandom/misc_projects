" Configuration file for vim

" Prevent modelines in files from being evaluated (avoids a potential
" security problem wherein a malicious user could write a hazardous
" modeline into a file) (override default value of 5)
set modelines=2
set backspace=indent,eol,start	" more powerful backspacing
set nocompatible	    " Use Vim defaults instead of 100% vi compatibility
set number              " Show line numbers
set viminfo='20,\"50	" Read/write a .viminfo file, don't store more than 50 lines of registers
set history=50		    " Keep 50 lines of command line history
set ruler		        " Show the cursor position all the time
set showmode            " Show which mode are we in
set showcmd		        " Show (partial) command in status line.
set hlsearch            " Highlight search matches
set noignorecase	    " Do case sensitive matching
set incsearch		    " Incremental search
set noautowrite		    " Automatically save before commands like :next and :make
set smarttab            " <BS> deletes a tab not only a space
set shiftwidth=4        " Number of space characters inserted for indentation
set expandtab           " Insert space characters whenever the tab key is pressed
set tabstop=4           " Number of space characters that will be inserted when tab is pressed
set smartindent		    " Always set autoindenting on
set autoindent		    " Always set autoindenting on
set showmatch		    " Show matching brackets.
set backupdir=~/tmp,/tmp  " Group backup files on this directories
set directory=.,~/tmp,/tmp  " Group swap files on this directories
set dictionary=~/.vim/wordlists/c.list,~/.vim/wordlists/php.list   "Wordlists for completion
set cpt=.,k,w,d
set background=dark
set encoding=utf-8
set winminheight=0
set winminwidth=0
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc

" Folding
set foldtext=""         " Don't show fold's first line
set foldmethod=indent
set nofoldenable

syntax on

" We know xterm-debian is a color terminal
if &term =~ "xterm-debian" || &term =~ "xterm-xfree86"
  set t_Co=16
  set t_Sf=[3%dm
  set t_Sb=[4%dm
endif

" Make p in Visual mode replace the selected text with the "" register.
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

if has("autocmd")
 " Enabled file type detection
 " Use the default filetype settings. If you also want to load indent files
 " to automatically do language-dependent indenting add 'indent' as well.
 filetype plugin on

endif " has ("autocmd")

" Some Debian-specific things
augroup filetype
  au BufRead reportbug.*		set ft=mail
  au BufRead reportbug-*		set ft=mail
augroup END

" Set paper size from /etc/papersize if available (Debian-specific)
if filereadable('/etc/papersize')
  let s:papersize = matchstr(system('/bin/cat /etc/papersize'), '\p*')
  if strlen(s:papersize)
    let &printoptions = "paper:" . s:papersize
  endif
  unlet! s:papersize
endif


"KEY MAPS"
""""""""""
"------------------------------------------------------------
"Space opens a fold recusively
nmap <Space> zA
"CR as in insert mode (break lines)
nmap <CR> i<CR><ESC>
"Y yanks to the end of the line as it should be
nmap Y y$
"Tab takes you to the next window:
nmap <Tab> _
nmap <S-Tab>  W_
nmap <S-Tab> :tabprevious<CR>
"' jumps to the precise location of a marks (`)
map ' `
"Apply the commands recorded in q on the vilual selection
vnoremap ` :normal @q<CR>
"--------------------------------------------------------------

"Open file same dir--------------------------------------------
"On the current window
map ,e :e <C-R>=expand("%:p:h") . "/" <CR>
"On a new vertical window
map ,v :vsp <C-R>=expand("%:p:h") . "/" <CR>
"On a new horizontal window
map ,h :sp <C-R>=expand("%:p:h") . "/" <CR>
"--------------------------------------------------------------

"Programming utils-------------------------------------------
" comment (#)
nmap sc 0i# <ESC>
vmap <silent> sc :s/^/# /<CR>:silent noh<CR>
" uncomment (#)
nmap su :s/^# //<CR>
vmap <silent> su :s/^# //<CR>:silent noh<CR>
"--------------------------------------------------------------

"Misc----------------------------------------------------------
"Line numbers ON/OFF
map ,n :set number!<CR>
"Read and write one block of text between vim sessions
nmap ,r   :r $HOME/.vimxfer<CR>
nmap ,w   :'a,.w! $HOME/.vimxfer<CR>
vmap ,r   c<esc>:r $HOME/.vimxfer<CR>
vmap ,w   :w! $HOME/.vimxfer<CR>
nmap <silent> ,, :silent noh<CR>
"-------------------------------------------------------------

highlight Folded ctermfg=darkmagenta

nmap <silent> <F12> :write<CR>
imap <silent> <F12> <ESC>:write<CR>

function! SuperTab()
    if (strpart(getline('.'),col('.')-2,1)=~'^\s\?$')
        return "\<Tab>"
    else
        return "\<C-n>"
    endif
endfunction
imap <Tab> <C-R>=SuperTab()<CR>

noremap <F2> :prev<CR>
noremap <F3> :n<CR>

map <DOWN> gj
map <UP> gk

map ,st i#include <stdio.h>int main(int argc,char **argv){return 0;}<ESC> 

function! Svndiff2()
    let dir = expand('%:p:h')
    let fn = expand('%')
    execute ":vert diffsplit" . dir . "/.svn/text-base/" . fn . ".svn-base"
    unlet fn dir
    return
endfunction

function! Svndiff()
    let fn = expand('%:p')
    new
    set ft=diff
    execute ":.!svn diff -r BASE " . fn
    unlet fn
    return
endfunction

function! Html_entities()
    execute ':%s/á/\&aacute;/g'
    execute ':%s/é/\&eacute;/g'
    execute ':%s/í/\&iacute;/g'
    execute ':%s/ó/\&oacute;/g'
    execute ':%s/ú/\&uacute;/g'
    execute ':w'
    execute ':next'
    return
endfunction

nmap ## :call Html_entities()<CR>
nmap <silent> ,D :call Svndiff()<CR>
nmap <silent> ,d :call Svndiff2()<CR>

" spell check
nmap ,spes :set spell spelllang=es<CR>
nmap ,spen :set spell spelllang=en<CR>

" tabs
nmap sn :tabnew
nmap sc :tabclose<CR>
nmap sj :tabnext<CR>
nmap sh :tabfirst<CR>
nmap sk :tabprevious<CR>
nmap sl :tablast<CR>
nmap st :tabs<CR>

" git commit file syntax file
autocmd BufNewFile,BufRead COMMIT_EDITMSG set filetype=gitcommit

colorscheme peachpuff

" highlight de las funciones de la glibc
let c_hi_identifiers = 'all'
let c_hi_libs = ['*'] 

map Y "+y$

" higliht cursor line
hi CursorLine term=none cterm=none ctermbg=0
set cursorline


" Octave Syntax
augroup filetypedetect
     au! BufRead,BufNewFile *.m setfiletype octave
augroup END 
