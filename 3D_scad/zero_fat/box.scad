$fn = 64;

// transducer
baffle_cutout_diameter = 91;
mounting_holes_diameter = 106;
transducer_position = [0, 0, 25];

// vents
vent_diameter = 22;
vent_length   = 70;
vent_tube_thickness = 2;
vent_z_distance_from_transducer=20;
vent_x_distance_from_center=20;
vent_smoothing_radius = 5;

// box
box_in_width  = 120; // dimensions to get to the 1.8 liter (+ transducer and vents)
box_in_height = 180;
box_in_depth  = 120;

box_side_walls_thickness = 30; // concrete thickness
round_edge_radius = 20;

// cast
pillar_width = 50;
wood_panel_thickness = 10;

// front back lids    
front_back_thickness = 20;

/////////////////////////////////////////////////////////////////////
// derived crap
box_out_width  = box_in_width + box_side_walls_thickness*2;
box_out_height = box_in_height + box_side_walls_thickness*2;
box_out_depth  = box_in_depth + box_side_walls_thickness*2;
pillar_length = box_out_depth;
vent_z_position = mounting_holes_diameter/2+vent_z_distance_from_transducer;


/////////////////////////////////////////////////////////////////////
// put objects in canvas
transducer4in();
///vents_pair(vent_diameter);
///
//concrete_walls();
//cast();

//union(){
//front_panel(baffle_cutout_diameter+front_back_thickness*2, vent_diameter);
//donut_transducer();
//}
//back_panel();

//vents_tubes_pair();


// HOW TO CALCULATE VOLUME
//probe($volume=true){ 
//    //transducer4in();  // the thing to measure
//    vents_pair(vent_diameter);
//    // the result, with measurements available
//    echo("volume=",volume," centroid=",centroid);
//    //%transducer4in();
//    vents_pair(vent_diameter);
//    // color("red") translate(centroid) sphere(r=1,$fn=32);
//}


//
module back_panel(){
    color("saddlebrown")
    // back
    difference(){
        // front panel
        translate([0, box_in_depth+front_back_thickness/2, 0])
        cube([box_out_width, front_back_thickness, box_out_height], center=true);

        // cutouts
        union(){
            translate([0, 0, 0]){
            four_round_pillars();
            }
        }
    }
}

module donut_vent_smoothing_positive(inner_hole_diameter, smoothing_radius){
    translate([0, smoothing_radius, 0])
    // vents' donut
    intersection(){
        rotate([90, 0, 0])
        rotate_extrude(convexity = 10)
        translate([inner_hole_diameter/2+smoothing_radius, 0, 0])
        circle(r = smoothing_radius, $fn = 100);

        rotate([90, 0, 0])
        cylinder(d=inner_hole_diameter+smoothing_radius*2, h=smoothing_radius*2);
    }
}

module donut_vent_smoothing_negative(inner_hole_diameter, smoothing_radius){
    difference(){
        color("green")
        rotate([90, 0, 0])
        cylinder(d=inner_hole_diameter+smoothing_radius*2, h=smoothing_radius*2, center=true);
        
        donut_vent_smoothing_positive(inner_hole_diameter, smoothing_radius);
    }
}

module donut_transducer(complete){
    // transducer's donut
        color("saddlebrown")
    intersection(){
        front_panel(baffle_cutout_diameter, vent_diameter);
        
    translate(transducer_position)
        intersection(){
        // donut - smooth baffle cutout
        rotate([90, 0, 0])
        rotate_extrude(convexity = 10)
        translate([baffle_cutout_diameter/2+front_back_thickness, 0, 0])
        circle(r = front_back_thickness, $fn = 100);
    
        rotate([90, 0, 0])
        cylinder(r=baffle_cutout_diameter/2+front_back_thickness, h=front_back_thickness*2, center=true);
        }
    }
}

module donut_vent(side, complete){
    x_sign = (side == 0) ? -1 : 1;
    if(complete==true){
         translate([x_sign*vent_x_distance_from_center, 0, -vent_z_position]){
            donut_one_vent();
        }
   }
    else{
    intersection(){
        intersection(){
            front_panel(baffle_cutout_diameter+front_back_thickness*2, vent_diameter);
            
            translate([x_sign*box_out_width/2, -front_back_thickness/2, 0])
            cube([box_out_width, front_back_thickness*2, box_out_height], center=true);
        }
    
        translate([x_sign*vent_x_distance_from_center, 0, -vent_z_position]){
            donut_one_vent();
        }
    }
    }
}


module donut_one_vent(){
    // vents' donut
    intersection(){
    rotate([90, 0, 0])
    rotate_extrude(convexity = 10)
    translate([vent_diameter/2+front_back_thickness, 0, 0])
    circle(r = front_back_thickness, $fn = 100);

    rotate([90, 0, 0])
    cylinder(d=vent_diameter+front_back_thickness*2, h=front_back_thickness*2, center=true);
    }
}


module front_panel(baffle_cutout_diameter, vent_diameter){
    color("saddlebrown")
    // front
    difference(){
        // front panel
        translate([0, -front_back_thickness/2, 0])
        cube([box_out_width, front_back_thickness, box_out_height], center=true);

        // cutouts
        union(){
            translate([0, -front_back_thickness*1.1, 0]){
            four_round_pillars();
            vents_pair(vent_diameter);
            translate(transducer_position){
                baffle_cutout(baffle_cutout_diameter, front_back_thickness*3);
                mounting_holes(mounting_holes_diameter, front_back_thickness*3);
            }
            }
        // smoothing vent outa right
        translate([vent_x_distance_from_center, -front_back_thickness, -vent_z_position]){
            donut_vent_smoothing_negative(vent_diameter, vent_smoothing_radius);}
        // smoothing vent out left
        translate([-vent_x_distance_from_center, -front_back_thickness, -vent_z_position]){
            donut_vent_smoothing_negative(vent_diameter, vent_smoothing_radius);}

        }
    }
}


/////////////////////////////////////////////////////////////////////
module cast(){
    // external cast
    four_round_pillars();
    wood_cast_external_panels();
    
    // internal cast
    four_internal_pillars();
    wood_cast_internal_panels();
}

module wood_cast_external_panels(){
    // external
    translate([-box_out_width/2-wood_panel_thickness/2, box_out_depth/2, 0])
    color("blue")
    cube([wood_panel_thickness, box_out_depth, box_out_height-round_edge_radius*2], center=true);
    
    translate([box_out_width/2+wood_panel_thickness/2, box_out_depth/2, 0])
    color("blue")
    cube([wood_panel_thickness, box_out_depth, box_out_height-round_edge_radius*2], center=true);
    
    translate([0, box_out_depth/2, box_out_height/2+wood_panel_thickness/2])
    rotate([0, 90, 0])
    color("blue")
    cube([wood_panel_thickness, box_out_depth, box_out_width-round_edge_radius*2], center=true);
 
    translate([0, box_out_depth/2, -(box_out_height/2+wood_panel_thickness/2)])
    rotate([0, 90, 0])
    color("blue")
    cube([wood_panel_thickness, box_out_depth, box_out_width-round_edge_radius*2], center=true);
}

module wood_cast_internal_panels(){
    // sides
    for(idx=[0:1:1])
    rotate([0, 180*idx, 0])
    translate([box_in_width/2-wood_panel_thickness/2, box_out_depth/2, 0])
    color("blue")
    cube([wood_panel_thickness, box_out_depth, box_in_height-pillar_width], center=true);

    // top/bottom
    for(idx=[0:1:1])
    rotate([0, 180*idx, 0])
    translate([0, box_out_depth/2, box_in_height/2-wood_panel_thickness/2])
    color("blue")
    cube([box_in_width-pillar_width, box_out_depth, wood_panel_thickness], center=true);
}


/////////////////////////////////////////////////////////////////////
module four_internal_pillars(){
    // top left
    translate([-box_in_width/2, 0, box_in_height/2])
    rotate([0, 90, 0])
    one_internal_pillar();
    // top right
    translate([box_in_width/2, 0, box_in_height/2])
    rotate([0, 180, 0])
    one_internal_pillar();

    // bottom right
    translate([box_in_width/2, 0, -box_in_height/2])
    rotate([0, -90, 0])
    one_internal_pillar();

    // bottom left
    translate([-box_in_width/2, 0, -box_in_height/2])
    one_internal_pillar();

}

module one_internal_pillar(){
    // one pillar
    difference(){
        // pillar
        translate([pillar_width/2, pillar_length/2, pillar_width/2])
        color("saddlebrown")
        cube([pillar_width, pillar_length, pillar_width], center=true);
        
        // space for wooden side
        for(idx=[0:1:1]){
            rotate([0, 90*idx])
            translate([0, pillar_length/2, pillar_width/4+pillar_width/2+2])
            color("purple")
            cube([wood_panel_thickness*2, pillar_length+4, pillar_width/2+4], center=true);
        }
    }

}


/////////////////////////////////////////////////////////////////////
// concrete walls
module concrete_walls(){
    // concrete walls
    color("gray")
    difference(){
        box_sides();
        translate([0, -4, 0])
        four_round_pillars();
    }
}


module box_sides(){
    difference(){
        // external shape
        translate([0, box_in_depth/2, 0])
        cube([box_out_width, box_in_depth, box_out_height], center= true);
        // internal volume
        translate([0, box_in_depth/2, 0])
        cube([box_in_width, box_in_depth+4, box_in_height], center= true);
    }
}


/////////////////////////////////////////////////////////////////////
// wooden cast for the concrete walls
module four_round_pillars(){
    for(idx=[0:2:3]){
    rotate([0, 90*idx, 0])
    translate([-box_out_width/2, 0, -box_out_height/2])
    one_round_pillar();
    }
    
    for(idx=[0:2:3]){
    rotate([0, 90*idx, 0])
    translate([-box_out_width/2, 0, box_out_height/2])
    rotate([0, 90, 0])
    one_round_pillar();
    }
}


module one_round_pillar(){
    cylinder_center_position = pillar_width/2/sqrt(2); 
    
    translate([round_edge_radius-cylinder_center_position, 0, round_edge_radius-cylinder_center_position])
    difference(){
        // pillar
        color("saddlebrown")
        translate([0, pillar_length/2, 0])
        cube([pillar_width, pillar_length, pillar_width], center=true);

        // round edge
        color("lime")
        translate([cylinder_center_position, pillar_length/2, cylinder_center_position])
        rotate([90,0,0]) // I want to build the model standing, like in reality 
        cylinder(d=round_edge_radius*2, h=pillar_length+4, center=true);
 
        // wood panels
        wood_panel_height = pillar_width / 2 + 4; // can be anything more than pillar_width/2 
        translate([-(round_edge_radius-cylinder_center_position)-wood_panel_thickness, 0, cylinder_center_position])
        translate([wood_panel_thickness, pillar_length/2, wood_panel_height/2]) // put the corner into 0,0,0
        cube([wood_panel_thickness*2, pillar_length+4, wood_panel_height], center=true);
    
        translate([cylinder_center_position, 0, -(round_edge_radius-cylinder_center_position)-wood_panel_thickness])
        translate([wood_panel_height/2, pillar_length/2, wood_panel_thickness]) // put the corner into 0,0,0
        cube([wood_panel_height, pillar_length+4, wood_panel_thickness*2], center=true);
    }
  }


/////////////////////////////////////////////////////////////////////
// vents
// reflecting sica recommendation on sizes
// positioning still to be decided
module vents_tubes_pair(){
    translate([-vent_x_distance_from_center, 0, -vent_z_position]){
        vent_tube();
    }
    translate([vent_x_distance_from_center, 0, -vent_z_position]){
        vent_tube();
    }

}

module vent_tube(){
    color("black")
    translate([0, vent_length/2, 0])
    difference(){
         rotate([90,0,0]) // I want to build the model standing, like in reality 
        cylinder(d=vent_diameter+vent_tube_thickness*2, h=vent_length, center=true);

        rotate([90,0,0]) // I want to build the model standing, like in reality 
        cylinder(d=vent_diameter, h=vent_length, center=true);
    }
}

module vents_pair(vent_diameter){
    translate([-vent_x_distance_from_center, 0, -vent_z_position]){
        vent(vent_diameter, vent_length);
    }
    translate([vent_x_distance_from_center, 0, -vent_z_position]){
        vent(vent_diameter, vent_length);
    }
}


module vent(diameter, length){
    color("black")
    translate([0, length/2, 0])
    rotate([90,0,0]) // I want to build the model standing, like in reality 
    cylinder(d=diameter, h=length, center=true);
}


/////////////////////////////////////////////////////////////////////
// transducer
// consists of a main body,
// a black circle indicating the front baffle cutout
// and the screw holes.
module transducer4in(){
    total_depth = 68.6;
    side = 105.5;
    
    translate(transducer_position) {
    // transducer main body
    intersection(){
        translate([0,total_depth/2.,0])
            color("silver")
            cube([side, total_depth, side], center=true);
        
        translate([0,total_depth/2.,0])
            rotate([90,0,0]) // 
            cylinder(d=mounting_holes_diameter+10, h=total_depth+2, center=true);
        
    }


    baffle_cutout(baffle_cutout_diameter, 1);
    mounting_holes(mounting_holes_diameter, 1);
    }
}



module baffle_cutout(baffle_cutout_diameter, height){
    rotate([90,0,0]){ // I want to build the model standing, like in reality 

        // internal cutout
        color("black")
        cylinder(d=baffle_cutout_diameter, h=height, center=true);
    }
}


module mounting_holes(mounting_holes_diameter, height){
    rotate([90,0,0]){ // I want to build the model standing, like in reality 
        color("black"){

        // screws holes
        for(idx=[1:1:4])
        rotate([0, 0, 45+(90*idx)])
        translate([mounting_holes_diameter/2., 0., 0])
        cylinder(d=5, h=height, $fn=32, center=true);
        }
    }
}
