ring_height = 20;
caps_height = 10;
capsule_diam = 6;
caps_rad = capsule_diam/2;

outer = 70;
inner = 60;

num_capsules=20;

/// RING
module RING(){
	rotate([0,0,360/2/num_capsules]) difference(){
		cylinder(ring_height,outer,outer,$fn=num_capsules);
		translate([0,0,-ring_height/2]) cylinder(ring_height*2,inner,inner,$fn=num_capsules);
	}
}

/// HOLES
module HOLE(){
	translate([-outer,-caps_height/2,((ring_height-caps_height)/2)]) cube([outer*2,caps_height,caps_height]);
}


module MICS(){ 
	for (i = [1:num_capsules/2]) {
		color("blue") rotate([0,0,360/num_capsules*i]) HOLE();
	}
}


/////////////
module MIC_RING(){
	difference(){
		RING();
		MICS();
	}
}


module CAPS_BAY(){
	difference(){
	color("red") cube([outer-inner,caps_height,caps_height]);
	translate([-(outer-inner)*0.25, caps_rad+(caps_height-capsule_diam)/2, (caps_rad/2+(caps_height-caps_rad)/2)]) rotate([0,90,0]) cylinder((outer-inner)*2,caps_rad,caps_rad,$fn=16);
	}
}

module CAPSULE(){
	color("black") translate([0, caps_rad+(caps_height-capsule_diam)/2, (caps_rad/2+(caps_height-caps_rad)/2)]) rotate([0,90,0]) cylinder((outer-inner),caps_rad,caps_rad,$fn=16);
}

////////////
MIC_RING();

CAPS_BAY();
CAPSULE();
