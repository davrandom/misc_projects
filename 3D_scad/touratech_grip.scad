// definitions
// lower ring
rint_l  = 6/2;
rext_l  = 13/2;
nint_l  = 24;
next_l  = 32;
h_l     = 2.5;

// mid ring
rint_m  = 11/2;
rext_m  = rext_l;
nint_m  = 6;
next_m  = next_l;
h_m     = 5;

// top ring - grip
rint_t  = rint_m;
rext_t  = 20/2;
nint_t  = 32;
next_t  = 4;
h_t     = 6;


// modules
module anillo(ha,r_out,r_in, n_out, n_in){
    difference(){
        cylinder(ha,r_out,r_out,false,$fn=n_out); //scanalatura
        translate([0,0,-0.5]){
        cylinder(ha*2,r_in,r_in,false,$fn=n_in); //scanalatura
        }
    }
    }


// do stuff
    union(){
// lower ring
anillo(h_l,rext_l,rint_l,next_l,nint_l);
translate([0,0,h_l]){
// mid ring
    anillo(h_m,rext_m,rint_m,next_m,nint_m);}
translate([0,0,h_m+h_l]){
// top crown
    union(){
        anillo(h_t,rext_t,rint_t,next_t,nint_t);
        rotate([0,0,45])
        anillo(h_t,rext_t,rint_t,next_t,nint_t);
    }
}
}