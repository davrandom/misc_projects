// all measures are in mm
$fn = 36*2;
// $fn = 12;

///////////////////////
// Params
face_cube_side = 115;
face_height = 7;
face_rounding = 8;

central_hole_diameter = 76;
central_hole_height   = 30;

smoothin_outer_diameter = face_cube_side - 4;
smoothin_height = face_height;

speaker_support_side = 86;

speaker_side = 83.6;
speaker_screws_distance = 62;
speaker_screws_depth = face_height;
speaker_screws_diameter = 3.2;

box_bolts_distance = 94;
box_bolts_depth = 20;
box_bolts_diameter = 3.0;

box_bolts_head_diameter = 5.80;
box_bolts_head_length = 3.5;

main_box_thickness = 7;
main_box_height = 80;
nut_rin = 3;
nut_rout= 6;
nut_thickness = 4;

back_hole_diameter = 50;
back_connector_recess_side = 58;
back_connector_recess_height = 3.5;
back_screws_distance = 44;

//////////
// Back //
//////////
// translate([140, 0, 0])
// difference(){
//     front_face_smooth();
//     union(){
//         front_bolts();
//         cylinder(d=back_hole_diameter, h=face_height*2); // main hole
//         cube([back_connector_recess_side, back_connector_recess_side, back_connector_recess_height], center=true);
//         translate([back_screws_distance/2, back_screws_distance/2, 0]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
//         translate([-back_screws_distance/2, back_screws_distance/2, 0]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
//         translate([back_screws_distance/2, -back_screws_distance/2, 0]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
//         translate([-back_screws_distance/2, -back_screws_distance/2, 0]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
//         }
//     }


//////////////
// Box body //
//////////////
//translate([face_cube_side, 0, 0]){
translate([0, 0, face_height*2+1]){
    main_body_with_bolts(want_nuts=true);
    }
//}

///////////
// Front //
///////////
// back border
// union(){
// difference(){
//     translate([0, 0, face_height+1])
//         main_body_with_bolts(want_nuts=false);
//     translate([0, 0, face_height+face_cube_side/2+face_height])
//         cube([face_cube_side+10, face_cube_side+10, face_cube_side],true);
//     }
//
// Front face
// difference(){
//     //alt// centered_rounded_cube(face_cube_side, face_cube_side, face_height, face_rounding);
//     front_face_smooth();
//     union(){
//         inner_hole();
//         front_bolts();
//         }
//     }
//
// // Speaker base
// difference(){
//     translate([0, 0, face_height]) centered_rounded_cube(speaker_support_side, speaker_support_side, face_height, face_rounding);
//     union(){
//         //alt// translate([0, 0, (face_height*2)-1]) centered_rounded_cube(speaker_support_side-1, speaker_support_side-1, face_height, face_rounding);
//         inner_hole();
//         speaker_screws();
//         }
//     }
// }

///////////////////////
// modules

module space_for_nut(r_out, thickness, rotation, extension){
    rotate([0,0,rotation]){
        rotate([0, 0, 90])
        cylinder(d=r_out, h=thickness, $fn=6);
        translate([0, -r_out/2., 0])
        cube([extension, r_out, thickness]);
    }
}

module nut(r_out, r_in, thickness){
    difference(){
    cylinder(d=r_out, h=thickness, $fn=6);
    translate([0,0,-1])
    cylinder(d=r_in, h=thickness+2, $fn=36);
    }
    }


module main_body_with_bolts(want_nuts){
    union(){
    difference(){
        main_box_body();

        union(){
            space_for_bolts_main_body();
            translate([0, 0, main_box_height-box_bolts_depth+1]) space_for_bolts_main_body();
        }
    }
    bolts_main_body(want_nuts);
    rotate([180, 0, 0]) translate([0, 0, -main_box_height])
    bolts_main_body(want_nuts);
    }
}

module main_box_body(){
    difference(){
        translate([-face_cube_side/2., -face_cube_side/2., 0]) rounded_cube(face_cube_side, face_cube_side, main_box_height, face_rounding*2);
        translate([-face_cube_side/2.+main_box_thickness, -face_cube_side/2.+main_box_thickness, -5]) rounded_cube(face_cube_side-main_box_thickness*2, face_cube_side-main_box_thickness*2, main_box_height+10, face_rounding*2);
    }
}


module space_for_bolts_main_body(){
    xy_trans = box_bolts_distance/2.;
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    translate([0,0,-1])
    box_bolts(xy_trans, z_trans, nut_rout, bolt_length+1);
}

module bolts_main_body(want_nuts){
    xy_trans = box_bolts_distance/2.;
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    difference(){
    box_bolts(xy_trans, z_trans, nut_rout, bolt_length/4.+nut_thickness);


    union(){
        translate([0, 0, -1])
            box_bolts(xy_trans, z_trans, bolt_diameter, bolt_length);
        if (want_nuts == true) {
            // making the space for nuts
            translate([xy_trans, xy_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, -135, 30);
            translate([-xy_trans, xy_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, -45, 30);
            translate([xy_trans, -xy_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, 135, 30);
            translate([-xy_trans, -xy_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, 45, 30);
            }
        }
    }
}


module front_bolts(){
    xy_trans = box_bolts_distance/2.;
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    box_bolts(xy_trans, z_trans, bolt_diameter, bolt_length);
    box_bolts(xy_trans, z_trans, box_bolts_head_diameter, box_bolts_head_length);
}


module speaker_screws(){
    translate([speaker_screws_distance/2, speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([-speaker_screws_distance/2, speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([speaker_screws_distance/2, -speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([-speaker_screws_distance/2, -speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);    
    }


module box_bolts(xy_trans, z_trans, bolt_diameter, bolt_length){
    translate([xy_trans, xy_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([-xy_trans, xy_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([xy_trans, -xy_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([-xy_trans, -xy_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    }


module front_face_smooth(){
    translate([0,0,face_height*1.15])  // careful, this 1.15 is a bit arbitrary...
    difference(){
        minkowski(){
            centered_rounded_cube(face_cube_side-2*face_rounding, face_cube_side-2*face_rounding, face_height, face_rounding);
        sphere(r=face_rounding);
        }
        translate([-face_cube_side/2., -face_cube_side/2., 0]) 
        cube(face_cube_side, face_cube_side, face_height*4);
    }
}

module inner_hole(){
    union(){
    cylinder(h=central_hole_height, d=central_hole_diameter);
    cylinder(h=face_height, d1=smoothin_outer_diameter, d2= central_hole_diameter);
    }
    }


module rounded_cube(x, y, z, r){
    hull(){
    translate([r, r, 0]) cylinder(r=r,h=z);
    translate([x-r, r, 0]) cylinder(r=r,h=z);
    translate([r, y-r, 0]) cylinder(r=r,h=z);
    translate([x-r, y-r, 0]) cylinder(r=r,h=z);
    }
    }


module centered_rounded_cube(x, y, z, r){
    translate([-x/2., -y/2., 0]) 
    hull(){
    translate([r, r, 0]) cylinder(r=r,h=z);
    translate([x-r, r, 0]) cylinder(r=r,h=z);
    translate([r, y-r, 0]) cylinder(r=r,h=z);
    translate([x-r, y-r, 0]) cylinder(r=r,h=z);
    }
    }


/////////////////////////////////////////////////////
// alternative smooth face
y = face_cube_side;
x = face_cube_side;
diameter = face_rounding*2;
radius = diameter/2.;


// adjust length because of smoothing
side_y = y - diameter;
side_x = x - diameter;

// uncomment this line: 
//centered_half_soap(side_x, side_y, diameter);


module centered_half_soap(side_x, side_y, diameter){
    translate([-x/2., -y/2., 0])
    half_soap(side_x, side_y, diameter);
}


module half_soap(side_x, side_y, diameter){
    difference(){
        soap(side_x, side_y, diameter);
        cube([x, y, diameter]);
    }
}


module soap(side_x, side_y, diameter){
    translate([radius, radius, 0])
    rotate([-90, 0, 0])
    hull(){
        four_cylinders(side_x, side_y, diameter);
        four_spheres(side_x, side_y, diameter);
    }
}


module four_spheres(side_x, side_y, diameter){ 
    sphere(d=diameter);
    translate([side_x,0,0]) sphere(d=diameter);
    translate([0,0,side_y]) sphere(d=diameter);
    translate([side_x,0,side_y]) sphere(d=diameter);
}


module four_cylinders(side_x, side_y, diameter){
    cylinder(d=diameter,h=side_y);
    rotate([0,90,0]) cylinder(d=diameter,h=side_x);
    
    translate([side_x,0,0])
    cylinder(d=diameter,h=side_y);
    
    translate([0,0,side_y])
    rotate([0,90,0]) cylinder(d=diameter,h=side_x);
}


