$fn = 24;


// bottom bar
bar_width = 20;
bar_length = 60;
bar_height = 8;

camera_screw_diameter = 2.5;
camera_screw_head_diameter = 5;
camera_screw_head_heigth = 3;

handle_screw_diameter = 3;
handle_screw_length = 30;

handle_bolt_diameter = 6;
handle_bolt_length = 3;
handle_bolt_height = handle_screw_diameter / 2;

difference(){
    %main_bar();
    camera_screw();
    handle_screw();
}
handle_screw();

module main_bar(){
    union(){
        // main bar
        translate([0, 0, bar_height/2])
        cube([bar_width, bar_length, bar_height], center=true);

        // screw support
        translate([0, bar_length/2, 0]) rotate([0, 0, 45]) 
        cylinder(d1=bar_width, d2=bar_width*2, h=bar_height, $fn=4);
    }
}

// camera screw
module camera_screw(){
    // screw body
    color([1,0,0,1]) translate([0, bar_length/2, -1])
    cylinder(d=camera_screw_diameter, h=bar_height+2);

    // screw head
    color([0,0,1,1]) translate([0, bar_length/2, -1])
    cylinder(d=camera_screw_head_diameter, h=camera_screw_head_heigth+1);
}

// handle screw
module handle_screw(){
    // screw
    color([1,0,0,1]) translate([0, -bar_length/2-1, bar_height/2]) rotate([-90, 0, 0])
    cylinder(d=handle_screw_diameter, h=handle_screw_length+1, $fn=32);
    
    // bolt
    y_displacement = -bar_length/2+handle_screw_length-handle_bolt_length-4;
    color([1,0,0,1]) translate([0, y_displacement, bar_height/2]) rotate([-90, 0, 0])
    cylinder(d=handle_bolt_diameter, h=handle_bolt_length, $fn=6);

    // opening for bolt
    color([0,0,1,1]) translate([0, -bar_length/2-handle_bolt_length/2+handle_screw_length-4, bar_height]) 
    cube([handle_bolt_diameter, handle_bolt_length, bar_height], center=true);

    // recess
    longest_dim = max([bar_width, bar_length, bar_height]);

    recess_side = 2;
    for (idx = [0:3]) {
        color([0,1,0,1])
        translate([0, -bar_length/2+recess_side/2-0.5, -recess_side/2+bar_height/2+recess_side/2])
        rotate([0, 45*idx, 0])
        cube([longest_dim, recess_side+1, recess_side], center=true);
    }
 
}

pillars_width = 0.2;
number_of_pillars = 27;
for (idx = [0:number_of_pillars]) {
    spacer = (bar_width-pillars_width) / number_of_pillars;
    
    color([1,1,0,1])
    translate([-bar_width/2 + spacer*idx, -bar_length/2, 0])
    cube([pillars_width, 2, bar_height]);
}

    //color([1,0,0,1]) translate([0, -bar_length/2+handle_screw_length-handle_bolt_height-3, bar_height/2]) rotate([-90, 0, 0])
    //cylinder(d=handle_bolt_diameter, h=handle_bolt_height, $fn=6);
