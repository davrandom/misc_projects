// supporto telefono / telecamera

base_larghezza = 20;
base_altezza = 8;
base_lunghezza = 40;


translate([0,-0.2,0])
female(base_larghezza, base_altezza, base_lunghezza, 0.0);


difference(){
    // main cube
    translate([0,base_altezza*0.5,0])
        cube([base_larghezza, base_altezza*1.5, base_lunghezza], center=true);
    
    // to obtain male, subtract female with some shrinking..
    female(base_larghezza, base_altezza, base_lunghezza, 0.8);
}

module female(base_larghezza, base_altezza, base_lunghezza, scala_interno){
    difference(){
        // main cube 
        cube([base_larghezza, base_altezza, base_lunghezza], center=true);

        // inner
        translate([0, base_altezza*0.05-scala_interno/4, base_lunghezza*0.1])
            cube([base_larghezza*0.7-scala_interno, base_altezza*0.4-scala_interno, base_lunghezza], center=true);
        
        // corridor
        translate([0, base_altezza*0.3-scala_interno/4, base_lunghezza*0.1])
            cube([base_larghezza*0.5-scala_interno, base_altezza*0.8-scala_interno, base_lunghezza], center=true);
    }
}



