

$fn=8;



// tube
faces_tube=8;
curve=22.5;
rot_tub=22.5; // rotation around its axis
inner_r=18;
outer_r=21;


cutter=47; // cube for intersection
disp=3; // displacement


// base
size=39;

// screws
screws=32;
dist=32;
margin=(size-dist)/2;




////////////////////////////////////////////////////////

module donut(radius,resol){
		rotate_extrude($fn=8){
			translate([curve,0,0]) rotate([0,0,rot_tub]) circle(radius,$fn=resol);
		}
}


// screws
module tornillos1(length,size){
	union(){
	translate([-length/2,margin+disp,dist/2]) rotate([0,90,0]) cylinder(length,size,size,$fn=8);
	translate([-length/2,margin+disp+32,dist/2]) rotate([0,90,0]) cylinder(length,size,size,$fn=8);
	translate([-length/2,margin+disp,-dist/2]) rotate([0,90,0]) cylinder(length,size,size,$fn=8);
	translate([-length/2,margin+disp+32,-dist/2]) rotate([0,90,0]) cylinder(length,size,size,$fn=8);
	}
}

module tornillos2(length,size){
	union(){
	translate([margin+disp,-length/2+5,dist/2]) rotate([90,0,0]) cylinder(length,size,size,$fn=8);
	translate([margin+disp+32,-length/2+5,dist/2]) rotate([90,0,0]) cylinder(length,size,size,$fn=8);
	translate([margin+disp,-length/2+5,-dist/2]) rotate([90,0,0]) cylinder(length,size,size,$fn=8);
	translate([margin+disp+32,-length/2+5,-dist/2]) rotate([90,0,0]) cylinder(length,size,size,$fn=8);
	}
}


module tubo(){
intersection(){
	difference(){

		//Main Donut
		donut(outer_r,faces_tube);

		//Air pipe
		donut(inner_r,faces_tube);

	}

# translate([0,0,-cutter/2]) cube (size = [cutter,cutter,cutter]);

}
}




////////////
// Rialzo
////////////
module rialzo(){
intersection(){

difference(){

	// outer
	translate([curve,-5,0]) rotate([90,rot_tub,180]) cylinder(5,outer_r,outer_r,false,$fn=faces_tube);

	// fan hole base
	# translate([curve,-5,0]) rotate([90,rot_tub,180]) cylinder(5,inner_r,inner_r,false,$fn=faces_tube);

}

translate([0,-10,-cutter/2]) cube (size = [cutter,cutter,cutter]);

}
}






///////
// Base with holes for screws
///////
module base(size,x1,y1,x2,y2,rot,rot2){
difference(){

	// Base with screws
	translate([x1,y1,-size/2]) rotate([rot,0,rot]) cube([5,size,size]);


	// empty base
	translate([x2,y2,0]) rotate([rot2,rot_tub,rot3]) cylinder(20,inner_r,inner_r,false,$fn=faces_tube);

}

}



difference(){
	tubo();
	tornillos1(length=8,size=4);
	tornillos2(length=8,size=4);
}

difference(){
	rialzo();
	tornillos2(length=8,size=4);
}

difference(){
	base(size,x1=-5,y1=disp,x2=-8,y2=curve,rot=0,rot2=90,rot3=90);
	tornillos1(length=15,size=2);
}	

difference(){
base(size,x1=disp,y1=-10,x2=curve,y2=-15,rot=90,rot2=90,rot3=180);
	#tornillos2(length=15,size=2);
}	



