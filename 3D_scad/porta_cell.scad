mobile_thickness = 11; // with cover
mobile_width     = 73.5;
mobile_height    = 146.5;

difference(){
    cube([mobile_width+5, mobile_height+6, mobile_thickness+2], center=true);
    union(){
        translate([0, 0, 6])  cube([mobile_width+6, mobile_height-2, mobile_thickness], center=true);
        translate([0, mobile_height/3., -6]) cylinder(d=45, h=20, center=true);
        translate([0, -mobile_height/3., -6])
        cylinder(d=45, h=20, center=true);

        central_holes();

        elastic_space(mobile_height/2.-5);
        elastic_space(-(mobile_height/2.-5));
        mobile_phone();
    }
}



module central_holes(){
    for (step = [-mobile_width*2/6, -mobile_width*1/6, mobile_width*2/6, mobile_width*1/6])
        translate([step, 0, 0])
        cylinder(d=8, h=20, center=true);

}


module elastic_space(displacement){
    translate([0, displacement, -3]){
        rotate([0, 90, 0]){
            cylinder(d=5, h=mobile_width+10, center=true, $fn=6);
            translate([0, 0, -(mobile_width+10)/2.]) rotate([0, 0, 180]) cube([10, 1, mobile_width+10]);
        }
    }
}

module mobile_phone(){
    cube([mobile_width, mobile_height, mobile_thickness], center=true);
}


//


