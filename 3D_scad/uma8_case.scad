// minidsp UMA8 case
// units: millimeters

// globals
$fn = 36; //*4;
$fn = 36*4;

// constants
// array case
array_radius = 90.0 / 2.0;
total_height = 25;
side_thickness = 5;
top_bot_thickness = 3;

// legs
legs_radius = 76.0 / 2.0;
legs_height = 15;
legs_hole = 3.0;

// vents
vent_length = (array_radius+side_thickness)*2+5;  // don't touch
vent_height = 15;
vent_thickness =  2;

// usb port
usbport_width = 15;


// ----------------------------
// external case
// bottom
external_radius = array_radius+1+side_thickness;
translate([0,0,-top_bot_thickness/2])
cylinder(h=top_bot_thickness, r1=external_radius-top_bot_thickness, r2=external_radius, center=true);  // external


// side
translate([0,0,total_height/2])
difference(){
    // main ring
    ring();
    
    // vents
    vents(60);
    
    // usb port
    rotate(a=45, v=[0,0,1])
    cube([usbport_width, vent_length, vent_height], center=true);
}


// supports for usb hole (to be manually removed)
intersection(){
    translate([0,0,total_height/2])
    ring();
    
    supports_for_usb_hole();
}



// ----------------------------
// modules

// main ring
module ring(){
    difference(){
        cylinder(h=total_height, r=array_radius+1+side_thickness, center=true);  // external
        cylinder(h=total_height+1, r=array_radius+1, center=true);  // internal
        }
    }


module supports_for_usb_hole(){
    number = 10;
    support_width = 0.4;
    
    translate([0,0,total_height/2])
    rotate(a=35, v=[0,0,1])
    for (idx = [1:number]){
            rotate(a=20.0*idx/number, v=[0,0,1])
            cube([support_width, vent_length, vent_height], center=true);
            }
    }

// legs / pillars
translate([0, legs_radius, 0]) pillar();
translate([0, -legs_radius, 0]) pillar();
translate([legs_radius, 0, 0]) pillar();
translate([-legs_radius, 0, 0]) pillar();

module pillar(){
    translate([0,0,legs_height/2])
    cylinder(h=legs_height, d=legs_hole+1, center=true);

    translate([0,0,(legs_height+5)/2])
    cylinder(h=legs_height+5, d=legs_hole-1, center=true);
    }

// vents
module vents(number){
    for (idx = [1:number]){
        rotate(a=360.0*idx/number, v=[0,0,1])
        cube([vent_thickness, vent_length, vent_height], center=true);
        }
    }

