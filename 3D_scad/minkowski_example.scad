$fn = 30;
module round() {
minkowski() {
children();
sphere(1);
}
}



module everything(){
    difference(){
    cube([6, 40, 10], center=true);
    translate([0,4,0]) cube([1.5, 38, 12], center=true);

   
    for(yps = [-12,-4,4,12])
    translate([0,yps,0]) cylinder(r=2,h=12, center=true);

}
}


// only at the very end do:
//round(){
everything();
//}
