$fn=12;


back = 40;
side = 50;
height = 40;

d_viti = 3;

// L object
difference(){
    // main cube
    cube([side, back, height]);
    // main cube difference -> gives L shape
    translate([8, 8, -1]) cube([side, back, height+2]);
    
    // fori gancio
    //translate([-1, 10, 5]) cube([12, 25, 30]);
    empty_h = 12;
    translate([-1, 10, 5]) cube([12, 25, empty_h]);
    translate([-1, 10, 5+empty_h+6]) cube([12, 25, empty_h]);

    // fori viti
    translate([side-5, -2, 6])
    rotate([-90, 0, 0])
    cylinder(d=d_viti, h=12);

    translate([side-5, -2, height-6])
    rotate([-90, 0, 0])
    cylinder(d=d_viti, h=12);
    
    // fori viti
    translate([side-11, -2, 6])
    rotate([-90, 0, 0])
    cylinder(d=d_viti, h=12);

    translate([side-11, -2, height-6])
    rotate([-90, 0, 0])
    cylinder(d=d_viti, h=12);
}


// supporti foro gancio
for (idx = [0:9]){
    translate([0, 9+ 3*idx, 0])
    cube([8, 0.4, height]);
}


difference(){
    // L che si aggancia al frame
    translate([27, -10, 0])
    cube([8, 10, 40]);

    // fori viti
    translate([25, -5, 10])
    rotate([0, 90, 0])
    cylinder(d=d_viti, h=12);

    translate([25, -5, height-10 ])
    rotate([0, 90, 0])
    cylinder(d=d_viti, h=12);
}


difference(){
    // L che si aggancia al frame
    y_displacement = 10+8;
    translate([27, -10+y_displacement, 0])
    cube([8, 10, 40]);

    // fori viti
    translate([25, -5+y_displacement, 10])
    rotate([0, 90, 0])
    cylinder(d=d_viti, h=12);

    translate([25, -5+y_displacement, height-10 ])
    rotate([0, 90, 0])
    cylinder(d=d_viti, h=12);
}

