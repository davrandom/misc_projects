// all measures are in mm
$fn = 36*2;
// $fn = 12;

///////////////////////
// Params
//

body    = false;
top     = true;
bottom  = false;

// interruttore
larg_switch = 19.4;
lung_switch = 12.4;
// presa elettrica
larg_plug = 27.5;
altez_plug = 31.5;

epsilon = 0.3;
nozzle_size = 0.4;

speaker_screws_diameter = 3.2;
box_long_side = 140 + speaker_screws_diameter *2;
box_short_side = 87 + epsilon*2;

trasformatore_lato_lungo = 100;

box_bolts_head_diameter = 5.80;
box_bolts_head_length = 1.5;

face_height = box_bolts_head_length + 1.;
face_rounding = 2;

smoothin_outer_diameter = box_long_side - 4;
smoothin_height = face_height;

speaker_support_side = 86;
speaker_side = 83.6; // reform this !
speaker_screws_distance = 62;
speaker_screws_depth = face_height;

box_bolts_distance = 94;
box_bolts_depth = 20;
box_bolts_diameter = 3.0;

box_thickness = 2.5;
box_height = 65;
nut_rin = 3;
nut_rout= 6;
nut_thickness = 4;

// foro potenziometro
pot_volume_diametro = 6;
pot_volume_altezza = 40;
// fori RCA
rca_diametro = 6;
rca_altezza  = 48;

x_trans = box_long_side  / 2. - speaker_screws_diameter - box_thickness/2. ;
y_trans = box_short_side / 2. - speaker_screws_diameter - box_thickness/2.;
 
back_hole_diameter = 50;
back_connector_recess_side = 58;
back_connector_recess_height = 3.5;
back_screws_distance = 44;

////////////
// BOTTOM //
////////////
largh_supporto = 3;
altezza_supporto = 5;
xleft_supporto = -box_long_side/2+nut_rout; 
xright_supporto = -box_long_side/2+nut_rout+trasformatore_lato_lungo+largh_supporto; 
if (bottom){

    translate([xleft_supporto,  0, altezza_supporto]) 
    cube([largh_supporto, box_short_side - box_thickness*2 - nut_rout*2, altezza_supporto], center=true);

    translate([xright_supporto, 0, altezza_supporto]) 
    cube([largh_supporto, box_short_side - box_thickness*2 - nut_rout*2, altezza_supporto], center=true);
    
    difference(){
        front_face_smooth();
        difference(){
            }
        union(){
            front_bolts();
    }
}
}

//////////////
// Box body //
//////////////
//difference(){

if (body){
    translate([0, 0, face_height*2+1]){
        difference(){
            main_body_with_bolts(want_nuts=true);
            
            // plug elettrica
            translate([xright_supporto+larg_plug/2, box_short_side/2, box_height/2])
            cube([larg_plug, 10, altez_plug], center=true);
           
            // on-off switch
            translate([box_long_side/2, 0, box_height/2])
            cube([10, larg_switch, lung_switch], center=true);
            
            // foro potenziometro volume
            translate([-box_long_side/2, 0, pot_volume_altezza+pot_volume_diametro/2])
            rotate([0, 90, 0])
            cylinder(box_thickness*2, d=pot_volume_diametro, center=true);


            // TODO aggiungere buchi RCA
            for (index=[0:3]) {
            translate([-box_long_side/2+44+14*index, box_short_side/2, rca_altezza+rca_diametro/2])
            rotate([90, 0, 0])
            cylinder(box_thickness*2, d=rca_diametro, center=true);
            }
            }

        // pilastri plug
        for (xnum = [-2:2]){
        translate([xright_supporto+larg_plug/2 + larg_plug/5*xnum, box_short_side/2 - box_thickness/2, box_height/2])
        cube([nozzle_size, box_thickness, altez_plug], center=true);   
        } 
        // pilastri switch
        for (ynum = [-1:1]){
            translate([box_long_side/2- box_thickness/2, larg_switch/3*ynum, box_height/2])
            cube([box_thickness, nozzle_size, lung_switch], center=true);
        } 
    }
}

// translate([-box_short_side/2, 0, box_short_side/2])
// cube([box_short_side*1.5, box_short_side, box_height], center=true);

// translate([box_long_side/2, -box_short_side/2, box_short_side/2])
// cube([box_short_side*1.5, box_short_side*0.5, box_height], center=true);
//}


///////////
//  TOP  //
///////////
// Front face
translate([0, box_short_side+20, 0]){
if (top){
    difference(){
        front_face_smooth();
        difference(){
            aerazione();
            }
        union(){
            front_bolts();
            }
    }
}
}



///////////////////////
// modules
module aerazione(){
    numof_ycube  = 2;
    numof_xcubes = 4;
    separazione = 13;
    window_side =  7;
    
    translate([0, -window_side/4])
    for (ynum = [-numof_ycube:numof_ycube]){
        translate([0, ynum*separazione, 0]){
            for (xnum = [-numof_xcubes:numof_xcubes]){
                translate([xnum*separazione,(xnum+numof_xcubes)%2*window_side/2, 0]){
                    rotate(45,0,0){
                        cube(window_side, window_side, window_side, center=true);
                    }
                }
            }
        }
    }
}


module space_for_nut(r_out, thickness, rotation, extension){
    rotate([0,0,rotation]){
        rotate([0, 0, 90])
        cylinder(d=r_out, h=thickness, $fn=6);
        translate([0, -r_out/2., 0])
        cube([extension, r_out, thickness]);
    }
}

module nut(r_out, r_in, thickness){
    difference(){
    cylinder(d=r_out, h=thickness, $fn=6);
    translate([0,0,-1])
    cylinder(d=r_in, h=thickness+2, $fn=36);
    }
    }


module main_body_with_bolts(want_nuts){

    union(){
    difference(){
        box_body();

        union(){
            space_for_bolts_main_body();
            translate([0, 0, box_height-box_bolts_depth+1]) space_for_bolts_main_body();
        }
    }
    bolts_main_body(want_nuts);
    rotate([180, 0, 0]) translate([0, 0, -box_height])
    bolts_main_body(want_nuts);
    }
}

module box_body(){
    difference(){
        translate([-box_long_side/2., -box_short_side/2., 0]) rounded_cube(box_long_side, box_short_side, box_height, face_rounding*2);
        translate([-box_long_side/2.+box_thickness, -box_short_side/2.+box_thickness, -5]) rounded_cube(box_long_side-box_thickness*2, box_short_side-box_thickness*2, box_height+10, face_rounding*2);
    }
}


module space_for_bolts_main_body(){
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    translate([0,0,-1])
    box_bolts(x_trans, y_trans, z_trans, nut_rout, bolt_length+1);
}

module bolts_main_body(want_nuts){
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    difference(){
    box_bolts(x_trans, y_trans, z_trans, nut_rout, bolt_length/4.+nut_thickness);


    union(){
        translate([0, 0, -1])
            box_bolts(x_trans, y_trans, z_trans, bolt_diameter, bolt_length);
        if (want_nuts == true) {
            // making the space for nuts
            translate([x_trans, y_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, -135, 30);
            translate([-x_trans, y_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, -45, 30);
            translate([x_trans, -y_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, 135, 30);
            translate([-x_trans, -y_trans, bolt_length/4.]) space_for_nut(nut_rout, nut_thickness, 45, 30);
            }
        }
    }
}


module front_bolts(){
    z_trans = 0;
    bolt_diameter = box_bolts_diameter;
    bolt_length = box_bolts_depth;

    box_bolts(x_trans, y_trans, z_trans, bolt_diameter, bolt_length);
}


module speaker_screws(){
    translate([speaker_screws_distance/2, speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([-speaker_screws_distance/2, speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([speaker_screws_distance/2, -speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);
    translate([-speaker_screws_distance/2, -speaker_screws_distance/2, face_height*2-speaker_screws_depth]) cylinder(d=speaker_screws_diameter, h=speaker_screws_depth);    
    }


module box_bolts(x_trans, y_trans, z_trans, bolt_diameter, bolt_length){
   
    translate([x_trans, y_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([-x_trans, y_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([x_trans, -y_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    translate([-x_trans, -y_trans, z_trans]) cylinder(d=bolt_diameter, h=bolt_length);
    }


module front_face_smooth(){
    translate([0,0,face_height*1.15])  // careful, this 1.15 is a bit arbitrary...
    difference(){
        minkowski(){
            centered_rounded_cube(box_long_side-2*face_rounding, box_short_side-2*face_rounding, face_height, face_rounding);
        sphere(r=face_rounding);
        }
        translate([-box_long_side/2., -box_short_side/2., 0]) 
        cube(box_long_side, box_short_side, face_height*4);
    }

        larg_rinforzo = 2;
        alt_rinforzo = 2;
        // rinforzo lato lungo
        translate([0, box_short_side/2. - larg_rinforzo/2 - box_thickness - epsilon, 3]){
        cube([box_long_side/2, larg_rinforzo, alt_rinforzo], center=true);
        }
        translate([0, -box_short_side/2. + larg_rinforzo/2 + box_thickness + epsilon, 3]){
        cube([box_long_side/2, larg_rinforzo, alt_rinforzo], center=true);
        }
        // rinforzo lato corto
        translate([-box_long_side/2. + larg_rinforzo/2 + box_thickness + epsilon, 0, 3]){
        cube([larg_rinforzo, box_short_side/2, alt_rinforzo], center=true);
        }
        translate([box_long_side/2. - larg_rinforzo/2 - box_thickness - epsilon, 0, 3]){
        cube([larg_rinforzo, box_short_side/2, alt_rinforzo], center=true);
        }
}

central_hole_diameter = 76;
central_hole_height   = 30;

module inner_hole(){
    union(){
    cylinder(h=central_hole_height, d=central_hole_diameter);
    cylinder(h=face_height, d1=smoothin_outer_diameter, d2= central_hole_diameter);
    }
    }


module rounded_cube(x, y, z, r){
    hull(){
    translate([r, r, 0]) cylinder(r=r,h=z);
    translate([x-r, r, 0]) cylinder(r=r,h=z);
    translate([r, y-r, 0]) cylinder(r=r,h=z);
    translate([x-r, y-r, 0]) cylinder(r=r,h=z);
    }
    }


module centered_rounded_cube(x, y, z, r){
    translate([-x/2., -y/2., 0]) 
    hull(){
    translate([r, r, 0]) cylinder(r=r,h=z);
    translate([x-r, r, 0]) cylinder(r=r,h=z);
    translate([r, y-r, 0]) cylinder(r=r,h=z);
    translate([x-r, y-r, 0]) cylinder(r=r,h=z);
    }
    }


/////////////////////////////////////////////////////
// alternative smooth face
y = box_long_side;
x = box_short_side;
diameter = face_rounding*2;
radius = diameter/2.;


// adjust length because of smoothing
side_y = y - diameter;
side_x = x - diameter;

// uncomment this line: 
//centered_half_soap(side_x, side_y, diameter);


module centered_half_soap(side_x, side_y, diameter){
    translate([-x/2., -y/2., 0])
    half_soap(side_x, side_y, diameter);
}


module half_soap(side_x, side_y, diameter){
    difference(){
        soap(side_x, side_y, diameter);
        cube([x, y, diameter]);
    }
}


module soap(side_x, side_y, diameter){
    translate([radius, radius, 0])
    rotate([-90, 0, 0])
    hull(){
        four_cylinders(side_x, side_y, diameter);
        four_spheres(side_x, side_y, diameter);
    }
}


module four_spheres(side_x, side_y, diameter){ 
    sphere(d=diameter);
    translate([side_x,0,0]) sphere(d=diameter);
    translate([0,0,side_y]) sphere(d=diameter);
    translate([side_x,0,side_y]) sphere(d=diameter);
}


module four_cylinders(side_x, side_y, diameter){
    cylinder(d=diameter,h=side_y);
    rotate([0,90,0]) cylinder(d=diameter,h=side_x);
    
    translate([side_x,0,0])
    cylinder(d=diameter,h=side_y);
    
    translate([0,0,side_y])
    rotate([0,90,0]) cylinder(d=diameter,h=side_x);
}


