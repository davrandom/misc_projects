// main plate
plate_width = 155;
plate_height = 40;
plate_thickness = 1.2;

ps2_diameter = 12;
ps2_distancex = 30;
ps2_xpos = 30;
ps2_ypos = 21.5;


usb2_w = 35;
usb2_h = 17.5;
usb2_x = 28;

usb3_w = 16;
usb3_h = 33;
usb3_x = 47;

vga_w = 33;
vga_h = 12;
vga_x = 66;
vga_y = 2.5;

eth_w = 16;
eth_h = 27;
eth_x = plate_width - eth_w - 4.5;
eth_y = 2;

vent_diameter = 3.8;

module main_plate(){
difference(){

    // main plate
    cube([plate_width, plate_height, plate_thickness]);

    // ps2
    $fn = 64;
    radius = ps2_diameter/2;
    translate([ps2_xpos+radius, ps2_ypos+radius, 0])
    cylinder(h=plate_thickness+2, d=12, center=false);


    // usb2
    translate([usb2_x, 2, 0])
    cube([usb2_w, usb2_h, plate_thickness+2]);

    // usb3
    translate([usb3_x, 2, 0])
    cube([usb3_w, usb3_h, plate_thickness+2]);

    // vga
    translate([vga_x, vga_y, 0])
    cube([vga_w, vga_h, plate_thickness+2]);

    // eth
    translate([eth_x, eth_y, 0])
    cube([eth_w, eth_h, plate_thickness+2]);
    
    // venting 1
    vent(3, 6);
    
    //venting 2
    translate([100, 0, 0])
    vent(5, 6);
    
}


module vent(repx, repy){
    repx = repx -1;
    repy = repy -1;
    diam = vent_diameter;
    for(idxx = [0:1:repx]){
        for(idxy = [0:1:repy]){
            translate([2+diam+(diam+2)*idxx, diam+(diam+2)*idxy, 0])
            rotate([0,0,45])
            cylinder(h=plate_thickness+2, d=3.8, center=false, $fn=4);
        }
        }
}
}

// support clips
sc_w = 6.5;
sc_h = 1.0;
sc_h2 = 3.0;
sc_thick = 0.8;
sc_distance_from_top = 1.0;

sc_x1 = 9.5;
sc_x2 = 66;
sc_x3 = plate_width - 18;

translate([sc_x1, -sc_h, plate_thickness-sc_distance_from_top-sc_thick])
//translate([0, -sc_h, 0])
cube([sc_w, sc_h2, sc_thick]);

translate([sc_x2, -sc_h, plate_thickness-sc_distance_from_top-sc_thick])
cube([sc_w, sc_h2, sc_thick]);

translate([sc_x3, -sc_h, plate_thickness-sc_distance_from_top-sc_thick])
cube([sc_w, sc_h2, sc_thick]);

//------------------------
// screws L thingy
l_height = 8;
l_thickness = 3;

screw_d = 2.8;
screw_x = 2.5;
screw_z = -3 + plate_thickness;

screw2_x = plate_width - 12.5 - screw_d;

difference(){
// L
translate([0, plate_height - l_thickness, -l_height + plate_thickness])
cube([plate_width - 9.5, l_thickness, l_height]);

// first screw
translate([screw_x + screw_d/2, plate_height,-screw_d/2 + screw_z])
rotate([90, 0, 0])
cylinder(h=l_thickness+1, d=screw_d, center=false, $fn=18);

// second screw
translate([screw2_x + screw_d/2, plate_height,-screw_d/2 + screw_z])
rotate([90, 0, 0])
cylinder(h=l_thickness+1, d=screw_d, center=false, $fn=18);
}


// call modules
main_plate();
