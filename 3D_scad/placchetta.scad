// dimensiones placa en mm
xp = 50.0; //largo
yp = 30.0; //ancho
zp = 0.8; //grosor

// dimensiones alas
xa = 5; //largo
ya = yp; //ancho
za = zp; //grosor

//distancia placa - alas
dd = 5;

// ventana
xw = 20;
yw = 15;
zw = zp*2;

// palos
h = 5; //altura palos
r_top = (xa-2)/2;
r_bottom = (xa-1)/2;
//posición en la ala
pxpole = xa/2;
pypole = 2;
// altura corte en el palito
zscan= 1;
// tamaño corte
ha = 1;

module ala(xpos,ypos,zpos){
    union(){
        translate([xpos,ypos,zpos]){
            cube([xa,ya,za],false); //ala
            translate([-dd-1,0,0]) cube([dd+2,dd+2,zp]); //connexión con placa
            
        }
    }
}


module placaconalas(){
union(){
    cube([xp,yp,zp],true);    
    ala(xp/2+dd,-yp/2,-zp/2);
    rotate([0,180,0]) ala(xp/2+dd,-yp/2,-zp/2);
    }
}

module anillo(ha,r_out,r_in){
    difference(){
        cylinder(ha,r_out,r_out,false,$fn=10); //scanalatura
        translate([0,0,-0.5]){
        cylinder(ha*2,r_in,r_in,false,$fn=10); //scanalatura
        }
    }
    }

module palitos(){
    difference(){
    translate([xp/2+dd+pxpole,yp/2-pypole,-zp/2]){
        cylinder(h,r_bottom,r_top,false,$fn=10); //palo
    }
    
    translate([xp/2+dd+pxpole,yp/2-pypole,+zp/2+zscan]){
        anillo(ha,r_top*2,r_top,false,$fn=10); //scanalatura
    }
    }

    
    rotate([0,180,0]){
            translate([xp/2+dd+pxpole,yp/2-pypole,+zp/2]){
                
                difference(){
            rotate([180,0,0]){    
                cylinder(h,r_bottom,r_top,false,$fn=10); //palo
            }
             translate([0,0,-zp/2-zscan*2]){
            anillo(ha,r_top*2,r_top,false,$fn=10); //scanalatura
        }
    }
          
      }
    }
    
   
    
}
 
 difference(){
 placaconalas();
cube([xw,yw,zw],true);
 }
 palitos();