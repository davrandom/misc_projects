$fn = 32;
// general parameters
grosor = 1;
bandw = 6;

// lens param
innerlens = 12;
outerlens = innerlens + bandw;

// box params
height = 40;
width = 25;
depth = 18;

module band(){
    // front
    translate([0,0,grosor/2]){
        cube([width,bandw,grosor], true);
    }

    // sides
    translate([-width/2,0,grosor/2+depth/2]){
        cube([grosor,bandw,depth+grosor],true);
    }
    translate([width/2,0,grosor/2+depth/2]){
        cube([grosor,bandw,depth+grosor],true);
    }
    
    // back
    translate([0,0,grosor/2+depth]){
        cube([width,bandw,grosor],true);
    }
}


/// top
difference(){
cylinder(d = outerlens, grosor, true);
cylinder(d = innerlens, grosor, true);
}

difference(){
band();
cylinder(d = innerlens, grosor, true);
}

/// bottom
translate([0,-height/2,0]){   
band();
}

// sideband
translate([width/2,-height/4,depth/2]){
cube([grosor,height/2,bandw], true);
}