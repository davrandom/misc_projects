import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
t = np.arange(0.0, 10.0, 0.001)

damp0 = 0.0
f0 = 0.20

s  = np.exp(-damp0*t)*np.sin(2*np.pi*f0*t)
ex = np.exp(-damp0*t)

xl, = plt.plot(t,ex, lw=1, color='gray')
l, = plt.plot(t,s, lw=2, color='red')
plt.axis([0, 10, -1, 1])

axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
axdamp  = plt.axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)

sfreq = Slider(axfreq, 'Freq', 0.1, 3.0, valinit=f0)
sdamp = Slider(axdamp, 'Damp', 0.0, 5.0, valinit=damp0)

def update(val):
    damp = sdamp.val
    freq = sfreq.val
    xl.set_ydata(np.exp(-damp*t))
    l.set_ydata(np.exp(-damp*t)*np.sin(2*np.pi*freq*t))
    fig.canvas.draw_idle()
sfreq.on_changed(update)
sdamp.on_changed(update)

resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
def reset(event):
    sfreq.reset()
    sdamp.reset()
button.on_clicked(reset)
#
plt.show()
