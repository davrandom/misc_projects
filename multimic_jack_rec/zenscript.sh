#!/bin/bash

# setting the number of mics 
# (to be passed to python script... I don't know how)
mic=$(zenity --scale --text="How many channels?" --value=32 --min-value=1 --max-value=180 --step=2)
echo $mic

while :
do

# first question
    start=$(zenity --question --text="Start recording?"; echo $?)
    echo "result:$start"

# if yes (yes==0) start script, and go to next question
# if no (no==1) close window

    if [ $start = "0" ]; then
        echo "running pyrec.py"
        echo "whatever" | zenity --text-info
        #python pyrec.py | zenity --text-info
    else
        echo "exit program"
        exit
    fi

    while :
    do
# second question
        restart=$(zenity --question --text="REstart recording?\n If you want to record just leave this window open"; echo $?)
        echo "result:$restart"

# if yes: killall jackrec and restart script
# if no: killall and go to start

        if [ $restart = "0" ]; then
            echo "killall and run again pyrec"
            killall -9 jack_rec
            echo "restarting pyrec" | zenity --text-info
            #python pyrec.py | zenity --text-info
        else
            echo "killall and go to first question: start"
            killall -9 jack_rec
            break
        fi

    done

done
