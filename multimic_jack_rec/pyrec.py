

import os

cha = 'system:capture_'


channelsA = ''
channelsB = ''

# divide the channels in two groups
for i in range(1,33):
	channelsA += cha + str(i) + ' '

for i in range(33,65):
	channelsB += cha + str(i) + ' '


# this for is only for testing... then we'll use the call to os.system without for
for i in range(1,2):
	os.system('jack_rec -f myfileA_'+str(i)+'.wav -B 65536 -d 60 ' + channelsA + ' &')
	os.system('jack_rec -f myfileB_'+str(i)+'.wav -B 65536 -d 60 ' + channelsB + ' &')


