import matplotlib.pyplot as pyp
import glob
import os, errno


def make_dir(dirname):
    try:
        os.makedirs(dirname)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


files_list = glob.glob('temperature*.txt')
plots_directory = 'plots'
make_dir(plots_directory)
images_list = glob.glob(os.path.join(plots_directory, 'temperature*.png'))

for idx, filename in enumerate(files_list):
    name = filename.split('.')[0]
    is_already_parsed = any(name in imgname for imgname in images_list)  # search for filename in image names list
    if is_already_parsed:
        continue
    
    filehandle = open(filename, 'r')
    lines = filehandle.readlines()
    
    lines = [line.replace(' ','') for line in lines]
    lines = [line.replace('\n','') for line in lines]
    lines = [line.split('\t') for line in lines]

    lines = [line for line in lines if not len(line)<2]  # keep only lines that make sense for us
                                                         # i.e.: don't keep incomplete lines

    hour = [line[0] for line in lines]
    # pick 5 hours
    num_of_ticks = 5
    step = int(len(hour)/num_of_ticks)
    xticks = [hour[idx*step] for idx in range(num_of_ticks)]
    xticks.append(hour[-1])
    xpos = [idx*step for idx in range(num_of_ticks)]
    xpos.append(len(hour))
    
    offset = -5  # set termometer offset
    temperature = [float(line[1])+offset for line in lines]
    
    pyp.plot(temperature)
    pyp.title(name.replace('_',' '))
    pyp.xticks(xpos, xticks)
    pyp.ylim([20,30])
    pyp.savefig(os.path.join(plots_directory, name+'.png'))
    pyp.show()






