import argparse
import scipy.io.wavfile as wf
from scipy.signal import fftconvolve as fftconv

# read arguments:
parser = argparse.ArgumentParser(description="Convolution between two wavs.")
parser.add_argument('-r', '--recorded', type=str, dest="infile_name", default="recorded.wav", help="Recorded signal. Default=recorded.wav")
parser.add_argument('-i', '--invswp', type=str, dest="inv_sweep_name", default="invsweep.wav", help="inverse sweep. Default=invsweep.wav")
parser.add_argument('-o', '--outfile', type=str, dest="out_ir_name", default="out.wav", help="Output filename. Default=out.wav")

args = parser.parse_args()


# read original inverse sweep
[fs, inv_sweep] = wf.read(inv_sweep_name);


# Deconvolve and do a first cut so as to remove non-linear part
# It adds initial zero pad at start to make sure enough latency
n = length(inv_sweep);
[fs2, input_sweep] = wf.read(infile_name);
#deconvolved_ir = fftconv(input_sweep,inv_sweep)((n-1):end);
deconvolved_ir = fftconv(input_sweep,inv_sweep);

# Write output files
normalization = max(abs(deconvolved_ir));
wavwrite(deconvolved_ir/normalization,fs,32,out_ir_name);




