import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import numpy as np

workspace_in_meters = 4

# draw the figure so the animations will work
fig = plt.figure(figsize=[10, 10])
plt.xlim([-workspace_in_meters, workspace_in_meters])
plt.ylim([-workspace_in_meters, workspace_in_meters])
fig.show()
fig.canvas.draw()

distance_between_two_points_in_meters = 0.68
x_circle1 = distance_between_two_points_in_meters/2.0
x_circle2 = -x_circle1


def plot_circle(x, y, r=0.05):
    patch = mpatch.Circle((x, y), radius=r, color='r')
    fig.gca().add_patch(patch)


def plot_arrow(x, y, angle, length=1):
    dx = np.cos(angle) * length
    dy = np.sin(angle) * length
    patch = mpatch.Arrow(x, y, dx, dy, width=0.1)
    fig.gca().add_patch(patch)


while True:
    fig.clear()
    # update canvas immediately
    plt.xlim([-workspace_in_meters, workspace_in_meters])
    plt.ylim([-workspace_in_meters, workspace_in_meters])

    # circles
    plot_circle(x_circle1, 0)  # circle 1
    plot_circle(x_circle2, 0)  # circle 2

    # source direction
    angle_rad_circle1 = np.random.random() * 2.0 * np.pi
    angle_rad_circle2 = np.random.random() * 2.0 * np.pi

    plot_arrow(x_circle1, 0, angle_rad_circle1)
    plot_arrow(x_circle2, 0, angle_rad_circle2)
    plt.pause(0.1)

    fig.canvas.draw()

