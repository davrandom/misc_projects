import moviepy.editor as mp
import os

root_folder = raw_input('Where do I have to search for videos?')
for (dirpath,dirname,filename) in os.walk(root_folder):
    #filenames = [name for name in filename if ('mov' in name or 'mp4' in name)]
    # getting all the files in fubfolder
    filenames = [name for name in filename if 'MOV' in name]
    if not filenames: continue
    if len(filenames) == 1:  # and not 'mp4' in filenames[0] :
        # in this folder there's only one file so the conly thing I can d is to convert it
        fname = filenames[0]
        decision = raw_input('Do you want to convert '+ fname + ' in folder ' +dirpath.split('/')[-1]+ '? Y/n')
        if decision == 'n':
            continue
        else:
            newfile = fname.split('.')[0] + '.mp4'
            print('Converting ' + fname + ' into ' + newfile )
            clip = mp.VideoFileClip(os.path.join(dirpath, fname))
            clip.write_videofile(os.path.join(dirpath, newfile))
    else:
        # if there is more than one file per folder I want to join them
        decision = raw_input('Do you want to join ' + str(filenames) +' in folder ' +dirpath.split('/')[-1]+ '? Y/n')
        if decision == 'n':
            continue
        else:
            newfname = dirpath.split('/')[-1] + '.mp4'
            print('Concatenating and converting clips into: ' + newfname)
            for idx,fname in enumerate(filenames):
                if idx == 0:
                    final_clip = mp.VideoFileClip(os.path.join(dirpath, fname))
                else:
                    clip_to_concatenate = mp.VideoFileClip(os.path.join(dirpath, fname))
                    final_clip = mp.concatenate_videoclips([final_clip, clip_to_concatenate])
            # write to file
            final_clip.write_videofile(os.path.join(dirpath, newfname))

